@extends('layout')
<link href="{{ asset('js/fancybox/jquery.fancybox.min.css') }}" rel="stylesheet"/>

@section('content')
    <div class="banner-global" style="background: url('images/banner-about.jpg') no-repeat center;">
        <div class="css-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="{{ URL::to('/') }}">Home</a></li>
                    <li>/</li>
                    <li><a href="{{ URL::to('/gallery-reviews') }}">Gallery & Reviews</a></li>
                </ul>
            </div>
        </div>
        <div class="tbl">
            <div class="cell">
                <div class="container">
                    <div class="title">Gallery & Reviews</div>
                </div>
            </div>
        </div>
    </div>
    <div class="css-gallery">
        <div class="bg-gallery">
            <div class="container">
                <div class="t1">Gallery</div>
                <div class="row row4">
                    <div class="col-md-4">
                        <div class="img-big">
                            <a href="{{ asset('images/gallery1.jpg') }}" data-fancybox="gallery">
                                <img src="{{ asset('images/gallery1.jpg') }}" alt="" title=""/>
                            </a>
                        </div>
                        <div class="row row0">
                            <div class="col-6">
                                <div class="img-small">
                                    <a href="{{ asset('images/gallery2.jpg') }}" data-fancybox="gallery"><img src="{{ asset('images/gallery2.jpg') }}" alt="" title=""/></a>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="img-small"><a href="{{ asset('images/gallery3.jpg') }}" data-fancybox="gallery"><img src="{{ asset('images/gallery3.jpg') }}" alt="" title=""/></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row row0">
                            <div class="col-6">
                                <div class="img-small"><a href="{{ asset('images/gallery4.jpg') }}" data-fancybox="gallery"><img src="{{ asset('images/gallery4.jpg') }}" alt="" title=""/></a></div>
                            </div>
                            <div class="col-6">
                                <div class="img-small">
                                    <a href="https://www.youtube.com/watch?v=uGRbHrO3OwE?autoplay=1" data-fancybox="gallery">
                                        <img src="{{ asset('images/gallery5.jpg') }}" alt="" title=""/>
                                        <img class="video" src="{{ asset('images/video.png') }}" alt="" title=""/>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="img-big"><a href="{{ asset('images/gallery6.jpg') }}" data-fancybox="gallery"><img src="{{ asset('images/gallery6.jpg') }}" alt="" title=""/></a></div>
                    </div>
                    <div class="col-md-4">
                        <div class="img-big">
                            <a href="https://www.youtube.com/watch?v=uGRbHrO3OwE?autoplay=1" data-fancybox="gallery">
                                <img src="{{ asset('images/gallery7.jpg') }}" alt="" title=""/>
                                <img class="video" src="{{ asset('images/video.png') }}" alt="" title=""/>
                            </a>
                        </div>
                        <div class="row row0">
                            <div class="col-6">
                                <div class="img-small"><a href="{{ asset('images/gallery8.jpg') }}" data-fancybox="gallery"><img src="{{ asset('images/gallery8.jpg') }}" alt="" title=""/></a></div>
                            </div>
                            <div class="col-6">
                                <div class="img-small"><a href="{{ asset('images/gallery9.jpg') }}" data-fancybox="gallery"><img src="{{ asset('images/gallery9.jpg') }}" alt="" title=""/></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-review">
            <div class="abs"><img src="{{ asset('images/petik.png') }}" alt="" title=""/></div>
            <div class="container">
                <div class="mb60">
                    <div class="row">
                        <div class="col-md-3" style="position: relative;">
                            <div class="t-review">Reviews</div>
                            <div class="abs-review">
                                <ul class="l-star">
                                    <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                    <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                    <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                    <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                    <li><img src="{{ asset('images/star1.png') }}" alt="" title=""/></li>
                                </ul>
                                <div class="num">4 out of 5</div>
                                <div class="total">125 ratings</div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="row row4">
                                <div class="col-md-3 mt90">
                                    <div class="img-review">
                                        <img src="{{ asset('images/review1.jpg') }}" alt="" title=""/>
                                        <img src="{{ asset('images/review2.jpg') }}" alt="" title=""/>
                                    </div>
                                </div>
                                <div class="col-md-3 mt60">
                                    <div class="img-review">
                                        <img src="{{ asset('images/review3.jpg') }}" alt="" title=""/>
                                        <img src="{{ asset('images/review4.jpg') }}" alt="" title=""/>
                                    </div>
                                </div>
                                <div class="col-md-3 mt30">
                                    <div class="img-review">
                                        <img src="{{ asset('images/review5.jpg') }}" alt="" title=""/>
                                        <img src="{{ asset('images/review6.jpg') }}" alt="" title=""/>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="img-review">
                                        <img src="{{ asset('images/review7.jpg') }}" alt="" title=""/>
                                        <img src="{{ asset('images/review8.jpg') }}" alt="" title=""/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-filter">
                    <div class="in-filter">
                        <div class="t">Filters</div>
                        <div class="pos-rel">
                            <select class="select-filter">
                                <option value="With images">With images</option>
                                <option value="With text">With text</option>
                            </select>
                            <div class="icon"><img src="{{ asset('images/select.png') }}" alt="" title=""/></div>
                        </div>
                    </div>
                </div>
                <div class="row row15">
                    <div class="col-6 col-md-3">
                        <div class="item-review">
                            <div>
                                <div class="slider-review">
                                    <div class="item"><img src="{{ asset('images/review.jpg') }}" alt="" title=""/></div>
                                    <div class="item"><img src="{{ asset('images/review.jpg') }}" alt="" title=""/></div>
                                    <div class="item"><img src="{{ asset('images/review.jpg') }}" alt="" title=""/></div>
                                </div>
                            </div>
                            <div class="pad-review no-bdr">
                                <div class="row row4">
                                    <div class="col-md-7 my-auto">
                                        <ul class="l-star">
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star1.png') }}" alt="" title=""/></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-5 my-auto">
                                        <div class="date">4 Apr 2020</div>
                                    </div>
                                </div>
                                <div class="nm">Raymond Lim</div>
                                <div class="t">Ipsum sodales sed congue egestas</div>
                                <div class="desc">
                                    <p>Integer duis nullam tempor, risus est, aenean enim. Arcu feugiat netus metus eget ipsum nisi, elementum, sed. Ac sed diam enim placerat ultricies amet. Nisl id lectus dui et eu nibh. Velit pellentesque ut blandit magna turpis.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="item-review">
                            <div class="mb20">
                                <div class="pad-review">
                                    <div class="row row4">
                                        <div class="col-md-7 my-auto">
                                            <ul class="l-star">
                                                <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                                <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                                <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                                <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                                <li><img src="{{ asset('images/star1.png') }}" alt="" title=""/></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-5 my-auto">
                                            <div class="date">4 Apr 2020</div>
                                        </div>
                                    </div>
                                    <div class="nm">Raymond Lim</div>
                                    <div class="t">Ipsum sodales sed congue egestas</div>
                                    <div class="desc h100">
                                        <p>Integer duis nullam tempor, risus est, aenean enim. Arcu feugiat netus metus eget ipsum nisi, elementum, sed. Ac sed diam enim placerat ultricies amet. Nisl id lectus dui et eu nibh. Velit pellentesque ut blandit magna turpis.</p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="pad-review">
                                    <div class="row row4">
                                        <div class="col-md-7 my-auto">
                                            <ul class="l-star">
                                                <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                                <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                                <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                                <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                                <li><img src="{{ asset('images/star1.png') }}" alt="" title=""/></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-5 my-auto">
                                            <div class="date">4 Apr 2020</div>
                                        </div>
                                    </div>
                                    <div class="nm">Raymond Lim</div>
                                    <div class="t">Ipsum sodales sed congue egestas</div>
                                    <div class="desc h100">
                                        <p>Integer duis nullam tempor, risus est, aenean enim. Arcu feugiat netus metus eget ipsum nisi, elementum, sed. Ac sed diam enim placerat ultricies amet. Nisl id lectus dui et eu nibh. Velit pellentesque ut blandit magna turpis.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="col-6 col-md-3">
                        <div class="item-review">
                            <div>
                                <div class="slider-review">
                                    <div class="item"><img src="{{ asset('images/review.jpg') }}" alt="" title=""/></div>
                                    <div class="item"><img src="{{ asset('images/review.jpg') }}" alt="" title=""/></div>
                                    <div class="item"><img src="{{ asset('images/review.jpg') }}" alt="" title=""/></div>
                                </div>
                            </div>
                            <div class="pad-review no-bdr">
                                <div class="row row4">
                                    <div class="col-md-7 my-auto">
                                        <ul class="l-star">
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star1.png') }}" alt="" title=""/></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-5 my-auto">
                                        <div class="date">4 Apr 2020</div>
                                    </div>
                                </div>
                                <div class="nm">Raymond Lim</div>
                                <div class="t">Ipsum sodales sed congue egestas</div>
                                <div class="desc">
                                    <p>Integer duis nullam tempor, risus est, aenean enim. Arcu feugiat netus metus eget ipsum nisi, elementum, sed. Ac sed diam enim placerat ultricies amet. Nisl id lectus dui et eu nibh. Velit pellentesque ut blandit magna turpis.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="item-review">
                            <div class="mb20">
                                <div class="pad-review">
                                    <div class="row row4">
                                        <div class="col-md-7 my-auto">
                                            <ul class="l-star">
                                                <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                                <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                                <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                                <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                                <li><img src="{{ asset('images/star1.png') }}" alt="" title=""/></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-5 my-auto">
                                            <div class="date">4 Apr 2020</div>
                                        </div>
                                    </div>
                                    <div class="nm">Raymond Lim</div>
                                    <div class="t">Ipsum sodales sed congue egestas</div>
                                    <div class="desc h100">
                                        <p>Integer duis nullam tempor, risus est, aenean enim. Arcu feugiat netus metus eget ipsum nisi, elementum, sed. Ac sed diam enim placerat ultricies amet. Nisl id lectus dui et eu nibh. Velit pellentesque ut blandit magna turpis.</p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="pad-review">
                                    <div class="row row4">
                                        <div class="col-md-7 my-auto">
                                            <ul class="l-star">
                                                <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                                <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                                <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                                <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                                <li><img src="{{ asset('images/star1.png') }}" alt="" title=""/></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-5 my-auto">
                                            <div class="date">4 Apr 2020</div>
                                        </div>
                                    </div>
                                    <div class="nm">Raymond Lim</div>
                                    <div class="t">Ipsum sodales sed congue egestas</div>
                                    <div class="desc h100">
                                        <p>Integer duis nullam tempor, risus est, aenean enim. Arcu feugiat netus metus eget ipsum nisi, elementum, sed. Ac sed diam enim placerat ultricies amet. Nisl id lectus dui et eu nibh. Velit pellentesque ut blandit magna turpis.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="item-review">
                            <div>
                                <div class="slider-review">
                                    <div class="item"><img src="{{ asset('images/review.jpg') }}" alt="" title=""/></div>
                                    <div class="item"><img src="{{ asset('images/review.jpg') }}" alt="" title=""/></div>
                                    <div class="item"><img src="{{ asset('images/review.jpg') }}" alt="" title=""/></div>
                                </div>
                            </div>
                            <div class="pad-review no-bdr">
                                <div class="row row4">
                                    <div class="col-md-7 my-auto">
                                        <ul class="l-star">
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star1.png') }}" alt="" title=""/></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-5 my-auto">
                                        <div class="date">4 Apr 2020</div>
                                    </div>
                                </div>
                                <div class="nm">Raymond Lim</div>
                                <div class="t">Ipsum sodales sed congue egestas</div>
                                <div class="desc">
                                    <p>Integer duis nullam tempor, risus est, aenean enim. Arcu feugiat netus metus eget ipsum nisi, elementum, sed. Ac sed diam enim placerat ultricies amet. Nisl id lectus dui et eu nibh. Velit pellentesque ut blandit magna turpis.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="item-review">
                            <div>
                                <div class="slider-review">
                                    <div class="item"><img src="{{ asset('images/review.jpg') }}" alt="" title=""/></div>
                                    <div class="item"><img src="{{ asset('images/review.jpg') }}" alt="" title=""/></div>
                                    <div class="item"><img src="{{ asset('images/review.jpg') }}" alt="" title=""/></div>
                                </div>
                            </div>
                            <div class="pad-review no-bdr">
                                <div class="row row4">
                                    <div class="col-md-7 my-auto">
                                        <ul class="l-star">
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star1.png') }}" alt="" title=""/></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-5 my-auto">
                                        <div class="date">4 Apr 2020</div>
                                    </div>
                                </div>
                                <div class="nm">Raymond Lim</div>
                                <div class="t">Ipsum sodales sed congue egestas</div>
                                <div class="desc">
                                    <p>Integer duis nullam tempor, risus est, aenean enim. Arcu feugiat netus metus eget ipsum nisi, elementum, sed. Ac sed diam enim placerat ultricies amet. Nisl id lectus dui et eu nibh. Velit pellentesque ut blandit magna turpis.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="item-review">
                            <div>
                                <div class="slider-review">
                                    <div class="item"><img src="{{ asset('images/review.jpg') }}" alt="" title=""/></div>
                                    <div class="item"><img src="{{ asset('images/review.jpg') }}" alt="" title=""/></div>
                                    <div class="item"><img src="{{ asset('images/review.jpg') }}" alt="" title=""/></div>
                                </div>
                            </div>
                            <div class="pad-review no-bdr">
                                <div class="row row4">
                                    <div class="col-md-7 my-auto">
                                        <ul class="l-star">
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star1.png') }}" alt="" title=""/></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-5 my-auto">
                                        <div class="date">4 Apr 2020</div>
                                    </div>
                                </div>
                                <div class="nm">Raymond Lim</div>
                                <div class="t">Ipsum sodales sed congue egestas</div>
                                <div class="desc">
                                    <p>Integer duis nullam tempor, risus est, aenean enim. Arcu feugiat netus metus eget ipsum nisi, elementum, sed. Ac sed diam enim placerat ultricies amet. Nisl id lectus dui et eu nibh. Velit pellentesque ut blandit magna turpis.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="item-review">
                            <div>
                                <div class="slider-review">
                                    <div class="item"><img src="{{ asset('images/review.jpg') }}" alt="" title=""/></div>
                                    <div class="item"><img src="{{ asset('images/review.jpg') }}" alt="" title=""/></div>
                                    <div class="item"><img src="{{ asset('images/review.jpg') }}" alt="" title=""/></div>
                                </div>
                            </div>
                            <div class="pad-review no-bdr">
                                <div class="row row4">
                                    <div class="col-md-7 my-auto">
                                        <ul class="l-star">
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star1.png') }}" alt="" title=""/></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-5 my-auto">
                                        <div class="date">4 Apr 2020</div>
                                    </div>
                                </div>
                                <div class="nm">Raymond Lim</div>
                                <div class="t">Ipsum sodales sed congue egestas</div>
                                <div class="desc">
                                    <p>Integer duis nullam tempor, risus est, aenean enim. Arcu feugiat netus metus eget ipsum nisi, elementum, sed. Ac sed diam enim placerat ultricies amet. Nisl id lectus dui et eu nibh. Velit pellentesque ut blandit magna turpis.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="item-review">
                            <div class="pad-review">
                                <div class="row row4">
                                    <div class="col-md-7 my-auto">
                                        <ul class="l-star">
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star1.png') }}" alt="" title=""/></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-5 my-auto">
                                        <div class="date">4 Apr 2020</div>
                                    </div>
                                </div>
                                <div class="nm">Raymond Lim</div>
                                <div class="t">Ipsum sodales sed congue egestas</div>
                                <div class="desc">
                                    <p>Integer duis nullam tempor, risus est, aenean enim. Arcu feugiat netus metus eget ipsum nisi, elementum, sed. Ac sed diam enim placerat ultricies amet. Nisl id lectus dui et eu nibh. Velit pellentesque ut blandit magna turpis.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="item-review">
                            <div class="pad-review">
                                <div class="row row4">
                                    <div class="col-md-7 my-auto">
                                        <ul class="l-star">
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star1.png') }}" alt="" title=""/></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-5 my-auto">
                                        <div class="date">4 Apr 2020</div>
                                    </div>
                                </div>
                                <div class="nm">Raymond Lim</div>
                                <div class="t">Ipsum sodales sed congue egestas</div>
                                <div class="desc">
                                    <p>Integer duis nullam tempor, risus est, aenean enim. Arcu feugiat netus metus eget ipsum nisi, elementum, sed. Ac sed diam enim placerat ultricies amet. Nisl id lectus dui et eu nibh. Velit pellentesque ut blandit magna turpis.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="item-review">
                            <div class="pad-review">
                                <div class="row row4">
                                    <div class="col-md-7 my-auto">
                                        <ul class="l-star">
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star1.png') }}" alt="" title=""/></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-5 my-auto">
                                        <div class="date">4 Apr 2020</div>
                                    </div>
                                </div>
                                <div class="nm">Raymond Lim</div>
                                <div class="t">Ipsum sodales sed congue egestas</div>
                                <div class="desc">
                                    <p>Integer duis nullam tempor, risus est, aenean enim. Arcu feugiat netus metus eget ipsum nisi, elementum, sed. Ac sed diam enim placerat ultricies amet. Nisl id lectus dui et eu nibh. Velit pellentesque ut blandit magna turpis.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="item-review">
                            <div class="pad-review">
                                <div class="row row4">
                                    <div class="col-md-7 my-auto">
                                        <ul class="l-star">
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star2.png') }}" alt="" title=""/></li>
                                            <li><img src="{{ asset('images/star1.png') }}" alt="" title=""/></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-5 my-auto">
                                        <div class="date">4 Apr 2020</div>
                                    </div>
                                </div>
                                <div class="nm">Raymond Lim</div>
                                <div class="t">Ipsum sodales sed congue egestas</div>
                                <div class="desc">
                                    <p>Integer duis nullam tempor, risus est, aenean enim. Arcu feugiat netus metus eget ipsum nisi, elementum, sed. Ac sed diam enim placerat ultricies amet. Nisl id lectus dui et eu nibh. Velit pellentesque ut blandit magna turpis.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
<script type="text/javascript" src="{{ asset('js/fancybox/jquery.fancybox.min.js') }}"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('.nav-gallery').addClass('active');

        $('[data-fancybox="gallery"]').fancybox({
            infobar : false,
            buttons : ['close'],
            loop: true,
        });

        $('.slider-review').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            centerMode: false,
            focusOnSelect: true,
            infinite: false,
        });
	});
</script>
@endsection