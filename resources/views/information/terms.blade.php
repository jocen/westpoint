@extends('layout')

@section('content')
    <div class="banner-global" style="background: url('images/banner-about.jpg') no-repeat center;">
        <div class="css-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="{{ URL::to('/') }}">Home</a></li>
                    <li>/</li>
                    <li><a href="{{ URL::to('/terms') }}">Terms & Conditions</a></li>
                </ul>
            </div>
        </div>
        <div class="tbl">
            <div class="cell">
                <div class="container">
                    <div class="title">Terms & Conditions</div>
                </div>
            </div>
        </div>
    </div>
    <div class="css-about">
        <div class="container">
            <div class="mb80">
                <div class="bdy">
                    <p>Id risus est pellentesque tristique cras malesuada ante varius. Tempor, mi elementum consectetur ut ante dignissim pulvinar sit. Amet nibh massa quam maecenas in sit. Velit, vel duis pretium arcu porttitor suspendisse viverra. Id gravida vitae aliquam ut.</p><br/>
                    <p>Sed sit nunc vel turpis posuere ac ipsum mollis. Ultrices nisi, posuere ultrices odio leo nunc elementum pretium. Ipsum viverra justo elementum lobortis sit duis massa id sed. Ipsum viverra justo elementum lobortis sit duis massa id sed. </p>
                    <p>Id risus est pellentesque tristique cras malesuada ante varius. Tempor, mi elementum consectetur ut ante dignissim pulvinar sit. Amet nibh massa quam maecenas in sit. Velit, vel duis pretium arcu porttitor suspendisse viverra. Id gravida vitae aliquam ut.</p><br/>
                    <p>Sed sit nunc vel turpis posuere ac ipsum mollis. Ultrices nisi, posuere ultrices odio leo nunc elementum pretium. Ipsum viverra justo elementum lobortis sit duis massa id sed. Ipsum viverra justo elementum lobortis sit duis massa id sed. </p>
                    <p>Id risus est pellentesque tristique cras malesuada ante varius. Tempor, mi elementum consectetur ut ante dignissim pulvinar sit. Amet nibh massa quam maecenas in sit. Velit, vel duis pretium arcu porttitor suspendisse viverra. Id gravida vitae aliquam ut.</p><br/>
                    <p>Sed sit nunc vel turpis posuere ac ipsum mollis. Ultrices nisi, posuere ultrices odio leo nunc elementum pretium. Ipsum viverra justo elementum lobortis sit duis massa id sed. Ipsum viverra justo elementum lobortis sit duis massa id sed. </p>
                    <p>Id risus est pellentesque tristique cras malesuada ante varius. Tempor, mi elementum consectetur ut ante dignissim pulvinar sit. Amet nibh massa quam maecenas in sit. Velit, vel duis pretium arcu porttitor suspendisse viverra. Id gravida vitae aliquam ut.</p><br/>
                    <p>Sed sit nunc vel turpis posuere ac ipsum mollis. Ultrices nisi, posuere ultrices odio leo nunc elementum pretium. Ipsum viverra justo elementum lobortis sit duis massa id sed. Ipsum viverra justo elementum lobortis sit duis massa id sed. </p>
                    <p>Id risus est pellentesque tristique cras malesuada ante varius. Tempor, mi elementum consectetur ut ante dignissim pulvinar sit. Amet nibh massa quam maecenas in sit. Velit, vel duis pretium arcu porttitor suspendisse viverra. Id gravida vitae aliquam ut.</p><br/>
                    <p>Sed sit nunc vel turpis posuere ac ipsum mollis. Ultrices nisi, posuere ultrices odio leo nunc elementum pretium. Ipsum viverra justo elementum lobortis sit duis massa id sed. Ipsum viverra justo elementum lobortis sit duis massa id sed. </p>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
<script type="text/javascript">
	$(document).ready(function() {

	});
</script>
@endsection