@extends('layout')

@section('content')
    <div class="banner-global" style="background: url('images/banner-about.jpg') no-repeat center;">
        <div class="css-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="{{ URL::to('/') }}">Home</a></li>
                    <li>/</li>
                    <li><a href="{{ URL::to('/about-us') }}">About Us</a></li>
                </ul>
            </div>
        </div>
        <div class="tbl">
            <div class="cell">
                <div class="container">
                    <div class="title">About Us</div>
                </div>
            </div>
        </div>
    </div>
    <div class="css-about">
        <div class="container">
            <div class="mb80">
                <div class="row">
                    <div class="col-md-6 my-auto">
                        <div class="img"><img src="{{ asset('images/about.jpg') }}" alt="" title=""/></div>
                    </div>
                    <div class="col-md-6 my-auto">
                        <div class="t1 mb80">About us</div>
                        <div class="bdy">
                            <p>Id risus est pellentesque tristique cras malesuada ante varius. Tempor, mi elementum consectetur ut ante dignissim pulvinar sit. Amet nibh massa quam maecenas in sit. Velit, vel duis pretium arcu porttitor suspendisse viverra. Id gravida vitae aliquam ut.</p><br/>
                            <p>Sed sit nunc vel turpis posuere ac ipsum mollis. Ultrices nisi, posuere ultrices odio leo nunc elementum pretium. Ipsum viverra justo elementum lobortis sit duis massa id sed. Ipsum viverra justo elementum lobortis sit duis massa id sed. </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mb80">
                <div class="t1">Our core values</div>
                <ul class="l-values">
                    <li>
                        <div class="img-values"><img src="{{ asset('images/values1.jpg') }}" alt="" title=""/></div>
                        <div class="abs-values">
                            <p>Id risus est pellentesque tristique cras malesuada ante varius.</p>
                        </div>
                    </li>
                    <li>
                        <div class="img-values"><img src="{{ asset('images/values2.jpg') }}" alt="" title=""/></div>
                        <div class="abs-values">
                            <p>Id risus est pellentesque tristique cras malesuada ante varius.</p>
                        </div>
                    </li>
                    <li>
                        <div class="img-values"><img src="{{ asset('images/values3.jpg') }}" alt="" title=""/></div>
                        <div class="abs-values">
                            <p>Id risus est pellentesque tristique cras malesuada ante varius.</p>
                        </div>
                    </li>
                </ul>
            </div>
            <div>
                <div class="t1">Our services</div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="item-services">
                            <a href="{{ URL::to('/services-detail') }}">
                                <div class="img-services"><img src="{{ asset('images/services1.jpg') }}" alt="" title=""/></div>
                                <div class="t-services">Service XYZ</div>
                                <div class="bdy-services">
                                    <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="item-services">
                            <a href="{{ URL::to('/services-detail') }}">
                                <div class="img-services"><img src="{{ asset('images/services2.jpg') }}" alt="" title=""/></div>
                                <div class="t-services">Service XYZ</div>
                                <div class="bdy-services">
                                    <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="item-services">
                            <a href="{{ URL::to('/services-detail') }}">
                                <div class="img-services"><img src="{{ asset('images/services3.jpg') }}" alt="" title=""/></div>
                                <div class="t-services">Service XYZ</div>
                                <div class="bdy-services">
                                    <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="item-services">
                            <a href="{{ URL::to('/services-detail') }}">
                                <div class="img-services"><img src="{{ asset('images/services1.jpg') }}" alt="" title=""/></div>
                                <div class="t-services">Service XYZ</div>
                                <div class="bdy-services">
                                    <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="item-services">
                            <a href="{{ URL::to('/services-detail') }}">
                                <div class="img-services"><img src="{{ asset('images/services2.jpg') }}" alt="" title=""/></div>
                                <div class="t-services">Service XYZ</div>
                                <div class="bdy-services">
                                    <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="item-services">
                            <a href="{{ URL::to('/services-detail') }}">
                                <div class="img-services"><img src="{{ asset('images/services3.jpg') }}" alt="" title=""/></div>
                                <div class="t-services">Service XYZ</div>
                                <div class="bdy-services">
                                    <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="link-services">
                    <a href="{{ URL::to('/services') }}"><button class="hvr-button black" type="button">See all services</button></a>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
<script type="text/javascript">
	$(document).ready(function() {
		$('.nav-about').addClass('active');
	});
</script>
@endsection