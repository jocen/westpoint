@extends('layout')

@section('content')

    <div class="css-home"> 
        <div class="slider-home">
            <div class="item">
                <div class="banner-home" style="background: url('images/banner-home.jpg') no-repeat center;">
                    <div class="abs-banner">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-xl-6">
                                    <div class="t-banner">Massa nisi aenean augue pharetra, vitae</div>
                                    <div class="bdy-banner">
                                        <p>Tortor magna ac vitae sit dolor nunc tristique eu mollis sagittis pulvinar lacinia enim molestie</p>
                                    </div>
                                    <div class="link">
                                        <a href="{{ URL::to('/appointment') }}"><button class="hvr-button" type="button">Book us now</button></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="banner-home" style="background: url('images/banner-home.jpg') no-repeat center;">
                    <div class="abs-banner">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-xl-6">
                                    <div class="t-banner">Massa nisi aenean augue pharetra, vitae</div>
                                    <div class="bdy-banner">
                                        <p>Tortor magna ac vitae sit dolor nunc tristique eu mollis sagittis pulvinar lacinia enim molestie</p>
                                    </div>
                                    <div class="link">
                                        <a href="{{ URL::to('/appointment') }}"><button class="hvr-button" type="button">Book us now</button></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="banner-home" style="background: url('images/banner-home.jpg') no-repeat center;">
                    <div class="abs-banner">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-xl-6">
                                    <div class="t-banner">Massa nisi aenean augue pharetra, vitae</div>
                                    <div class="bdy-banner">
                                        <p>Tortor magna ac vitae sit dolor nunc tristique eu mollis sagittis pulvinar lacinia enim molestie</p>
                                    </div>
                                    <div class="link">
                                        <a href="{{ URL::to('/appointment') }}"><button class="hvr-button" type="button">Book us now</button></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="banner-home" style="background: url('images/banner-home.jpg') no-repeat center;">
                    <div class="abs-banner">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-xl-6">
                                    <div class="t-banner">Massa nisi aenean augue pharetra, vitae</div>
                                    <div class="bdy-banner">
                                        <p>Tortor magna ac vitae sit dolor nunc tristique eu mollis sagittis pulvinar lacinia enim molestie</p>
                                    </div>
                                    <div class="link">
                                        <a href="{{ URL::to('/appointment') }}"><button class="hvr-button" type="button">Book us now</button></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-home">
            <div class="img1"><img src="{{ asset('images/home1.jpg') }}" alt="" title=""/></div>
            <div class="img2"><img src="{{ asset('images/home2.jpg') }}" alt="" title=""/></div>
            <div class="abs1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="t1">NEW ARRIVAL</div>
                            <div class="t2">In adipiscing integer hendrerit</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="abs2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 offset-md-6">
                            <div class="pl70">
                                <div class="t1">For Family</div>
                                <div class="t2">In adipiscing integer hendrerit</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-about">
            <div class="img"><img src="{{ asset('images/about.png') }}" alt="" title=""/></div>
            <div class="img2"><img src="{{ asset('images/about2.jpg') }}" alt="" title=""/></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <div class="t1">About us</div>
                        <div class="bdy">
                            <p>Id risus est pellentesque tristique cras malesuada ante varius. Tempor, mi elementum consectetur ut ante dignissim pulvinar sit. Amet nibh massa quam maecenas in sit. Velit, vel duis pretium arcu porttitor suspendisse viverra. Id gravida vitae aliquam ut.</p><br/>
                            <p>Sed sit nunc vel turpis posuere ac ipsum mollis. Ultrices nisi, posuere ultrices odio leo nunc elementum pretium. Ipsum viverra justo elementum lobortis sit duis massa id sed. Ipsum viverra justo elementum lobortis sit duis massa id sed.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-choose">
            <div class="container">
                <div class="bdr-choose">
                    <div class="title-global">Why choose us</div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="in">
                                <div class="img"><img src="{{ asset('images/choose1.png') }}" alt="" title=""/></div>
                                <div class="t1">> 2,000</div>
                                <div class="t2">total deals transacted</div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="in">
                                <div class="img"><img src="{{ asset('images/choose2.png') }}" alt="" title=""/></div>
                                <div class="t1">> 2,000</div>
                                <div class="t2">total deals transacted</div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="in">
                                <div class="img"><img src="{{ asset('images/choose3.png') }}" alt="" title=""/></div>
                                <div class="t1">> 2,000</div>
                                <div class="t2">total deals transacted</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-service">
            <div class="container">
                <div class="title-global white">Featured services</div>
                <div class="slider-home-services">
                    <div class="item">
                        <a href="{{ URL::to('/services-detail') }}">
                            <div class="tbl">
                                <div class="cell img"><img src="{{ asset('images/service1.jpg') }}" alt="" title=""/></div>
                                <div class="cell text">
                                    <div class="t1">Service & Maintenance</div>
                                    <div class="bdy">
                                        <p>Tincidunt senectus sapien leo pulvinar et nunc elementum felis egestas.</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="{{ URL::to('/services-detail') }}">
                            <div class="tbl">
                                <div class="cell img"><img src="{{ asset('images/service2.jpg') }}" alt="" title=""/></div>
                                <div class="cell text">
                                    <div class="t1">Service & Maintenance</div>
                                    <div class="bdy">
                                        <p>Tincidunt senectus sapien leo pulvinar et nunc elementum felis egestas.</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="{{ URL::to('/services-detail') }}">
                            <div class="tbl">
                                <div class="cell img"><img src="{{ asset('images/service3.jpg') }}" alt="" title=""/></div>
                                <div class="cell text">
                                    <div class="t1">Service & Maintenance</div>
                                    <div class="bdy">
                                        <p>Tincidunt senectus sapien leo pulvinar et nunc elementum felis egestas.</p>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="mobile-services">
                    <div class="row">
                        <div class="col-md-6 col-lg-4">
                            <a href="{{ URL::to('/services-detail') }}">
                                <div class="tbl">
                                    <div class="cell img"><img src="{{ asset('images/service1.jpg') }}" alt="" title=""/></div>
                                    <div class="cell text">
                                        <div class="t1">Service & Maintenance</div>
                                        <div class="bdy">
                                            <p>Tincidunt senectus sapien leo pulvinar et nunc elementum felis egestas.</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <a href="{{ URL::to('/services-detail') }}">
                                <div class="tbl">
                                    <div class="cell img"><img src="{{ asset('images/service2.jpg') }}" alt="" title=""/></div>
                                    <div class="cell text">
                                        <div class="t1">Service & Maintenance</div>
                                        <div class="bdy">
                                            <p>Tincidunt senectus sapien leo pulvinar et nunc elementum felis egestas.</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <a href="{{ URL::to('/services-detail') }}">
                                <div class="tbl">
                                    <div class="cell img"><img src="{{ asset('images/service3.jpg') }}" alt="" title=""/></div>
                                    <div class="cell text">
                                        <div class="t1">Service & Maintenance</div>
                                        <div class="bdy">
                                            <p>Tincidunt senectus sapien leo pulvinar et nunc elementum felis egestas.</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <a href="{{ URL::to('/services-detail') }}">
                                <div class="tbl">
                                    <div class="cell img"><img src="{{ asset('images/service1.jpg') }}" alt="" title=""/></div>
                                    <div class="cell text">
                                        <div class="t1">Service & Maintenance</div>
                                        <div class="bdy">
                                            <p>Tincidunt senectus sapien leo pulvinar et nunc elementum felis egestas.</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <a href="{{ URL::to('/services-detail') }}">
                                <div class="tbl">
                                    <div class="cell img"><img src="{{ asset('images/service2.jpg') }}" alt="" title=""/></div>
                                    <div class="cell text">
                                        <div class="t1">Service & Maintenance</div>
                                        <div class="bdy">
                                            <p>Tincidunt senectus sapien leo pulvinar et nunc elementum felis egestas.</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <a href="{{ URL::to('/services-detail') }}">
                                <div class="tbl">
                                    <div class="cell img"><img src="{{ asset('images/service3.jpg') }}" alt="" title=""/></div>
                                    <div class="cell text">
                                        <div class="t1">Service & Maintenance</div>
                                        <div class="bdy">
                                            <p>Tincidunt senectus sapien leo pulvinar et nunc elementum felis egestas.</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="text-link">
                    <a href="{{ URL::to('/services') }}">Browse all services <img class="img1" src="{{ asset('images/arrow.png') }}" alt="" title=""/> <img class="img2" src="{{ asset('images/arrow2.png') }}" alt="" title=""/></a>
                </div>
            </div>
        </div>
        <div class="bg-featured">
            <div class="container">
                <div class="title-global">Featured cars</div>
                <div class="slider-featured">
                    <div class="item">
                        <div class="bg"><img src="{{ asset('images/featured1.jpg') }}" alt="" title=""/></div>
                        <div class="pad">
                            <div class="t1">Featured Car</div>
                            <div class="t2">drive the future</div>
                            <div class="img"><img src="{{ asset('images/featured1-1.jpg') }}" alt="" title=""/></div>
                            <div class="link">
                                <a href="{{ URL::to('/used-cars-detail') }}"><button class="hvr-button black" type="button">Enquire now</button></a>
                            </div>
                            <ul class="l-featured">
                                <li><img src="{{ asset('images/correct.png') }}" alt="" title=""/>Maximum comfort and sheer luxury.</li>
                                <li><img src="{{ asset('images/correct.png') }}" alt="" title=""/>Et faucibus et dapibus odio dolor volutpat.</li>
                                <li><img src="{{ asset('images/correct.png') }}" alt="" title=""/>> 5 years left on COE</li>
                            </ul>
                        </div>
                    </div>
                    <div class="item">
                        <div class="bg"><img src="{{ asset('images/featured2.jpg') }}" alt="" title=""/></div>
                        <div class="pad">
                            <div class="t1">Featured Car</div>
                            <div class="t2">for family</div>
                            <div class="img"><img src="{{ asset('images/featured2-2.jpg') }}" alt="" title=""/></div>
                            <div class="link">
                                <a href="{{ URL::to('/used-cars-detail') }}"><button class="hvr-button black" type="button">Enquire now</button></a>
                            </div>
                            <ul class="l-featured">
                                <li><img src="{{ asset('images/correct.png') }}" alt="" title=""/>Maximum comfort and sheer luxury.</li>
                                <li><img src="{{ asset('images/correct.png') }}" alt="" title=""/>Et faucibus et dapibus odio dolor volutpat.</li>
                                <li><img src="{{ asset('images/correct.png') }}" alt="" title=""/>> 5 years left on COE</li>
                            </ul>
                        </div>
                    </div>
                    <div class="item">
                        <div class="bg"><img src="{{ asset('images/featured3.jpg') }}" alt="" title=""/></div>
                        <div class="pad">
                            <div class="t1">Featured Car</div>
                            <div class="t2">drive the future</div>
                            <div class="img"><img src="{{ asset('images/featured3-3.jpg') }}" alt="" title=""/></div>
                            <div class="link">
                                <a href="{{ URL::to('/used-cars-detail') }}"><button class="hvr-button black" type="button">Enquire now</button></a>
                            </div>
                            <ul class="l-featured">
                                <li><img src="{{ asset('images/correct.png') }}" alt="" title=""/>Maximum comfort and sheer luxury.</li>
                                <li><img src="{{ asset('images/correct.png') }}" alt="" title=""/>Et faucibus et dapibus odio dolor volutpat.</li>
                                <li><img src="{{ asset('images/correct.png') }}" alt="" title=""/>> 5 years left on COE</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="text-link">
                    <a href="{{ URL::to('/used-cars') }}">Browse all used cars  <img class="img1" src="{{ asset('images/arrow2.png') }}" alt="" title=""/> <img class="img2" src="{{ asset('images/arrow.png') }}" alt="" title=""/></a>
                </div>
            </div>
        </div>
        <div class="bg-testi" style="background: url('images/bg-testi.jpg') no-repeat center;">
            <div class="container">
                <div class="title-global white">What our customers say</div>
                <div class="slider-testi">
                    <div class="item">
                        <div class="in">
                            <div class="bdy-testi">
                                <p class="mb15">Id risus est pellentesque tristique cras malesuada ante varius. Tempor, mi elementum consectetur ut ante dignissim pulvinar sit. Amet nibh massa quam maecenas in sit. Velit, vel duis pretium arcu porttitor suspendisse viverra. Id gravida vitae aliquam ut.</p>
                                <p>Anonymous</p>
                                <p>5 August 2021</p>
                            </div>
                            <ul class="l-star">
                                <li class="active"><i class="fas fa-star"></i></li>
                                <li class="active"><i class="fas fa-star"></i></li>
                                <li class="active"><i class="fas fa-star"></i></li>
                                <li class="active"><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                            </ul>
                        </div>
                    </div>
                    <div class="item">
                        <div class="in">
                            <div class="bdy-testi">
                                <p class="mb15">Id risus est pellentesque tristique cras malesuada ante varius. Tempor, mi elementum consectetur ut ante dignissim pulvinar sit. Amet nibh massa quam maecenas in sit. Velit, vel duis pretium arcu porttitor suspendisse viverra. Id gravida vitae aliquam ut.</p>
                                <p>Anonymous</p>
                                <p>5 August 2021</p>
                            </div>
                            <ul class="l-star">
                                <li class="active"><i class="fas fa-star"></i></li>
                                <li class="active"><i class="fas fa-star"></i></li>
                                <li class="active"><i class="fas fa-star"></i></li>
                                <li class="active"><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                            </ul>
                        </div>
                    </div>
                    <div class="item">
                        <div class="in">
                            <div class="bdy-testi">
                                <p class="mb15">Id risus est pellentesque tristique cras malesuada ante varius. Tempor, mi elementum consectetur ut ante dignissim pulvinar sit. Amet nibh massa quam maecenas in sit. Velit, vel duis pretium arcu porttitor suspendisse viverra. Id gravida vitae aliquam ut.</p>
                                <p>Anonymous</p>
                                <p>5 August 2021</p>
                            </div>
                            <ul class="l-star">
                                <li class="active"><i class="fas fa-star"></i></li>
                                <li class="active"><i class="fas fa-star"></i></li>
                                <li class="active"><i class="fas fa-star"></i></li>
                                <li class="active"><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                            </ul>
                        </div>
                    </div>
                    <div class="item">
                        <div class="in">
                            <div class="bdy-testi">
                                <p class="mb15">Id risus est pellentesque tristique cras malesuada ante varius. Tempor, mi elementum consectetur ut ante dignissim pulvinar sit. Amet nibh massa quam maecenas in sit. Velit, vel duis pretium arcu porttitor suspendisse viverra. Id gravida vitae aliquam ut.</p>
                                <p>Anonymous</p>
                                <p>5 August 2021</p>
                            </div>
                            <ul class="l-star">
                                <li class="active"><i class="fas fa-star"></i></li>
                                <li class="active"><i class="fas fa-star"></i></li>
                                <li class="active"><i class="fas fa-star"></i></li>
                                <li class="active"><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                            </ul>
                        </div>
                    </div>
                    <div class="item">
                        <div class="in">
                            <div class="bdy-testi">
                                <p class="mb15">Id risus est pellentesque tristique cras malesuada ante varius. Tempor, mi elementum consectetur ut ante dignissim pulvinar sit. Amet nibh massa quam maecenas in sit. Velit, vel duis pretium arcu porttitor suspendisse viverra. Id gravida vitae aliquam ut.</p>
                                <p>Anonymous</p>
                                <p>5 August 2021</p>
                            </div>
                            <ul class="l-star">
                                <li class="active"><i class="fas fa-star"></i></li>
                                <li class="active"><i class="fas fa-star"></i></li>
                                <li class="active"><i class="fas fa-star"></i></li>
                                <li class="active"><i class="fas fa-star"></i></li>
                                <li><i class="fas fa-star"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-partner">
            <div class="container">
                <div class="title-global">Our Partners</div>
                <div class="slider-partner">
                    <div class="item">
                        <img src="{{ asset('images/partner.png') }}" alt="" title=""/>
                    </div>
                    <div class="item">
                        <img src="{{ asset('images/partner.png') }}" alt="" title=""/>
                    </div>
                    <div class="item">
                        <img src="{{ asset('images/partner.png') }}" alt="" title=""/>
                    </div>
                    <div class="item">
                        <img src="{{ asset('images/partner.png') }}" alt="" title=""/>
                    </div>
                    <div class="item">
                        <img src="{{ asset('images/partner.png') }}" alt="" title=""/>
                    </div>
                    <div class="item">
                        <img src="{{ asset('images/partner.png') }}" alt="" title=""/>
                    </div>
                    <div class="item">
                        <img src="{{ asset('images/partner.png') }}" alt="" title=""/>
                    </div>
                    <div class="item">
                        <img src="{{ asset('images/partner.png') }}" alt="" title=""/>
                    </div>
                    <div class="item">
                        <img src="{{ asset('images/partner.png') }}" alt="" title=""/>
                    </div>
                    <div class="item">
                        <img src="{{ asset('images/partner.png') }}" alt="" title=""/>
                    </div>
                    <div class="item">
                        <img src="{{ asset('images/partner.png') }}" alt="" title=""/>
                    </div>
                    <div class="item">
                        <img src="{{ asset('images/partner.png') }}" alt="" title=""/>
                    </div>
                    <div class="item">
                        <img src="{{ asset('images/partner.png') }}" alt="" title=""/>
                    </div>
                    <div class="item">
                        <img src="{{ asset('images/partner.png') }}" alt="" title=""/>
                    </div>
                    <div class="item">
                        <img src="{{ asset('images/partner.png') }}" alt="" title=""/>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-app">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 my-auto order-2 order-md-1">
                        <div class="title-global">Download our app</div>
                        <div class="bdy">
                            <p>Amet sed magna enim elit elementum imperdiet pellentesque vestibulum. Pellentesque vulputate egestas lacus donec elementum molestie turpis. Elementum sed sed pellentesque tellus. Luctus eros, semper sit consequat amet eget eget convallis malesuada. Sit quis habitasse praesent est imperdiet nec. </p>
                        </div>
                        <ul class="l-app">
                            <li><a href="#"><img src="{{ asset('images/google-play.png') }}" alt="" title=""/></a></li>
                            <li><a href="#"><img src="{{ asset('images/apple.png') }}" alt="" title=""/></a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 my-auto order-1 order-md-2">
                        <div class="img"><img src="{{ asset('images/phone.png') }}" alt="" title=""/></div>
                        <div class="img2"><img src="{{ asset('images/phone2.png') }}" alt="" title=""/></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-email" style="background: url('images/bg-email.jpg') no-repeat center;">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 offset-md-5 col-lg-6 offset-lg-6">
                        <div class="in">
                            <form>
                                <div class="t1">Stay up to date to our latest offers.</div>
                                <div class="t2">Join our mailing list</div>
                                <input type="text" class="form-control" placeholder="Enter your email address"/>
                                <div class="link">
                                    <button class="hvr-button" type="button" data-bs-toggle="modal" data-bs-target="#modal-success">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-success" class="modal fade modal-global" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="pad-pop">
                    <div class="pad-bdy">
                        <div class="img-success">
                            <img src="{{ asset('images/signup.png') }}" alt="" title=""/>
                        </div>
                        <div class="t-pop">Successful!</div>
                        <div class="t-pop2">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                        </div>
                        <div class="btn-pop">
                            <a href="{{ URL::to('/') }}">
                                <button class="hvr-button" type="button">Ok, Got it</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
<script type="text/javascript">
	$(document).ready(function() {
        $('.slider-home').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: false,
            centerMode: false,
            focusOnSelect: true,
            infinite: false,
        });

        $('.slider-featured').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: false,
            arrows: false,
            centerMode: false,
            focusOnSelect: true,
            infinite: false,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: true
                    }
                }
            ]
        });

        $('.slider-testi').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            dots: false,
            arrows: false,
            centerMode: false,
            focusOnSelect: true,
            infinite: false,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        arrows: true,
                        slidesToShow: 1,
                    }
                }
            ]
        });
        $('.slider-partner').slick({
            slidesToShow: 5,
            slidesToScroll: 5,
            dots: true,
            arrows: false,
            centerMode: false,
            focusOnSelect: true,
            infinite: false,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    }
                }
            ]
        });

        $('.slider-home-services').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            centerMode: false,
            focusOnSelect: true,
            infinite: false,
        });

        $('.nav-home').addClass('active');

		$('header').addClass('abs');
	});
</script>
@endsection