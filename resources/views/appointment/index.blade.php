@extends('layout')

@section('content')
    <div class="css-appo">
        <div class="container">
            <ul class="css-breadcrumb">
                <li><a href="{{ URL::to('/') }}">Home</a></li>
                <li>/</li>
                <li><a href="{{ URL::to('/appointment') }}">Appointment Form</a></li>
            </ul>
        </div>
        <div class="pos-rel">
            <div class="img-appo"><img src="{{ asset('images/appo.png') }}" alt="" title=""/></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-lg-7">
                        <form class="css-form" action="{{ URL::to('/appointment-confirm') }}">
                            <div class="title">Book an Appointment</div>
                            <div class="bdy">
                                <p>Montes, aenean ut magna tempus amet, non eu tincidunt morbi. Lacus, rhoncus, feugiat quisque posuere velit quis. Sed volutpat et leo feugiat aenean eleifend praesent. Proin mauris vulputate adipiscing lacus consequat. Commodo neque malesuada rutrum ante. Ornare commodo eros, feugiat vitae potenti condimentum. </p>
                            </div>
                            <div class="form-group">
                                <label id="appointment-type">*Appointment Type</label>
                                <div class="box-select">
                                    <div class="input-select"><span class="text-select">Workshop Services</span></div>
                                    <div class="img-select"><img src="{{ asset('images/select.png') }}" alt="" title=""/></div>
                                    <div class="option-select">
                                        <ul>
                                            <li><a data-box="workshop" data-select="Workshop Services" class="active">Workshop Services</a></li>
                                            <li><a data-box="book" data-select="Book a Viewing Session (Car Dealership)">Book a Viewing Session (Car Dealership)</a></li>
                                            <li><a data-box="product" data-select="Product Purchase">Product Purchase</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="box-form-appo" id="workshop">
                                <div class="mb40">
                                    <div class="title-appo">Vehicle Information</div>
                                    <div class="form-group">
                                        <label id="plate-number-work">*Car Plate Number</label>
                                        <input class="form-control" id="plate-number-work" name="plate-number-work" placeholder="SJL 8900P" type="text" required="" />
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label id="car-make-work">*Car Make</label>
                                                <input class="form-control" id="car-make-work" name="car-make-work" placeholder="Toyota" type="text" required="" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label id="car-model-work">*Car Model</label>
                                                <input class="form-control" id="car-model-work" name="car-model-work" placeholder="Camry" type="text" required="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="title-appo">Contact Information</div>
                                    <div class="form-group">
                                        <label id="name-work">*Name</label>
                                        <input class="form-control" id="name-work" name="name-work" value="" type="text" required="" />
                                    </div>
                                    <div class="form-group">
                                        <label id="phone-work">*Contact Number</label>
                                        <input class="form-control only-number" id="phone-work" name="phone-work" type="text" value="" required="" />
                                    </div>
                                    <div class="form-group">
                                        <label id="email-work">*Email</label>
                                        <input class="form-control" id="email-work" name="email-work" type="email" placeholder="example@email.com" required="" />
                                    </div> 
                                    <div class="form-group form-date">
                                        <label>*Select Date</label>
                                        <div class="pos-rel">
                                            <input class="form-control date" type="text" required="" readonly="" name="date-work"/>
                                            <div class="abs"><img src="{{ asset('images/date.png') }}" alt="" title=""/></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label id="time-slot-work">*Select Time Slot</label>
                                        <ul class="l-time">
                                            <li><a>11:00AM</a></li>
                                            <li><a>12:00PM</a></li>
                                            <li><a>02:00PM</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="button-appo">
                                    <button class="hvr-button" type="submit">Book now</button>
                                </div>
                            </div>
                            <div class="box-form-appo" id="book">
                                <div class="mb40">
                                    <div class="form-group">
                                        <label id="agent-book">*Are you doing a trade-in?</label>
                                        <ul class="css-radio">
                                            <li>
                                                <a>
                                                    <input type="radio" id="trade-1" value="" name="section_trade[]" checked="">
                                                    <label for="trade-1">Yes</label>
                                                </a>
                                            </li>
                                            <li>
                                                <a>
                                                    <input type="radio" id="trade-2" value="" name="section_trade[]">
                                                    <label for="trade-2">No</label>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="mb40">
                                    <div class="title-appo">Vehicle Information</div>
                                    <div class="form-group">
                                        <label id="plate-number-book">*Car Plate Number</label>
                                        <input class="form-control" id="plate-number-book" name="plate-number-book" placeholder="SJL 8900P" type="text" required="" />
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label id="car-make-book">*Car Make</label>
                                                <input class="form-control" id="car-make-book" name="car-make-book" placeholder="Toyota" type="text" required="" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label id="car-model-book">*Car Model</label>
                                                <input class="form-control" id="car-model-book" name="car-model-book" placeholder="Camry" type="text" required="" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label id="current-car-book">*Current Car Mileage</label>
                                        <input class="form-control" id="current-car-book" name="current-car-book" placeholder="SJL 8900P" type="text" required="" />
                                    </div>
                                    <div class="form-group">
                                        <label id="agent-book">Agent / Parallel Importer Unit</label>
                                        <ul class="css-radio">
                                            <li>
                                                <a>
                                                    <input type="radio" id="agent-1" value="" name="agent[]" checked="">
                                                    <label for="agent-1">Agent Unit</label>
                                                </a>
                                            </li>
                                            <li>
                                                <a>
                                                    <input type="radio" id="agent-2" value="" name="agent[]">
                                                    <label for="agent-2">Parallel Importer Unit</label>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div>
                                    <div class="title-appo">Contact Information</div>
                                    <div class="form-group">
                                        <label id="name-book">*Name</label>
                                        <input class="form-control" id="name-book" name="name-book" value="" type="text" required="" />
                                    </div>
                                    <div class="form-group">
                                        <label id="phone-book">*Contact Number</label>
                                        <input class="form-control only-number" id="phone-book" name="phone-book" type="text" value="" required="" />
                                    </div>
                                    <div class="form-group">
                                        <label id="email-book">*Email</label>
                                        <input class="form-control" id="email-book" name="email-book" type="email" placeholder="example@email.com" required="" />
                                    </div>
                                    <div class="form-group text">
                                        <label id="nric-book">NRIC / FIN / Passport</label>
                                        <div class="txt-small">For NRIC / FIN, please only enter the last 4 characters of your NRIC/FIN, eg. 123A</div>
                                        <input class="form-control" id="nric-book" name="nric-book" type="email" placeholder="example@email.com" required="" />
                                    </div>
                                </div>
                                <div class="button-appo">
                                    <button class="hvr-button" type="submit">Book now</button>
                                </div>
                            </div>
                            <div class="box-form-appo" id="product">
                                <div class="mb40">
                                    <div class="title-appo">Vehicle Information</div>
                                    <div class="form-group">
                                        <label id="plate-number-product">*Car Plate Number</label>
                                        <input class="form-control" id="plate-number-product" name="plate-number-product" placeholder="SJL 8900P" type="text" required="" />
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label id="car-make-product">*Car Make</label>
                                                <input class="form-control" id="car-make-product" name="car-make-product" placeholder="Toyota" type="text" required="" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label id="car-model-product">*Car Model</label>
                                                <input class="form-control" id="car-model-product" name="car-model-product" placeholder="Camry" type="text" required="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="title-appo">Contact Information</div>
                                    <div class="form-group">
                                        <label id="name-product">*Name</label>
                                        <input class="form-control" id="name-product" name="name-product" value="" type="text" required="" />
                                    </div>
                                    <div class="form-group">
                                        <label id="phone-product">*Contact Number</label>
                                        <input class="form-control only-number" id="phone-product" name="phone-product" type="text" value="" required="" />
                                    </div>
                                    <div class="form-group">
                                        <label id="email-product">*Email</label>
                                        <input class="form-control" id="email-product" name="email-product" type="email" placeholder="example@email.com" required="" />
                                    </div>
                                    <div class="form-group form-date">
                                        <label>*Select Date</label>
                                        <div class="pos-rel">
                                            <input class="form-control date" type="text" required="" readonly="" name="date-product"/>
                                            <div class="abs"><img src="{{ asset('images/date.png') }}" alt="" title=""/></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label id="time-slot-product">*Select Time Slot</label>
                                        <ul class="l-time">
                                            <li><a>11:00AM</a></li>
                                            <li><a>12:00PM</a></li>
                                            <li><a>02:00PM</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="button-appo">
                                    <button class="hvr-button" type="submit">Book now</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function() {
        $('ul.l-time li a').click(function(event) {
            $('ul.l-time li a').removeClass('active');
            $(this).addClass('active');
        });

        $(".date").datepicker({
            changeMonth: true,
            minDate: 0,
            changeYear: true,
            dateFormat: 'dd MM yy',
            beforeShowDay: $.datepicker.noWeekends
        });

        $('.input-select').click(function(event) {
            $('.option-select').addClass('open');
        });

        $('#workshop').addClass('active');

        $('.option-select ul li a').click(function(event) {
            var getSelect = $(this).attr('data-select');
            var getBox = $(this).attr('data-box');
            $('.option-select ul li a').removeClass('active');
            $(this).addClass('active');
            $('.option-select').removeClass('open');
            $('.text-select').html(getSelect);
            $('.box-form-appo').removeClass('active');
            $(".box-form-appo#" + getBox).addClass('active');
            return;
        });
    });
</script>
@endsection