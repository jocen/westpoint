@extends('layout')

@section('content')
    <div class="css-appo">
        <div class="container">
            <ul class="css-breadcrumb">
                <li><a href="{{ URL::to('/') }}">Home</a></li>
                <li>/</li>
                <li><a href="{{ URL::to('/appointment') }}">Appointment Form</a></li>
                <li>/</li>
                <li><a href="{{ URL::to('/appointment-confirm') }}">Appointment Confirmation</a></li>
            </ul>
            <div class="img-success"><img src="{{ asset('images/success.png') }}" alt="" title=""/></div>
            <div class="row">
                <div class="col-md-8">
                    <div class="t-thank">Thank you for booking an appointment with us.</div>
                    <div class="bdy-thank">
                        <p>Montes, aenean ut magna tempus amet, non eu tincidunt morbi.</p>
                        <br/>
                        <p>Lacus, rhoncus, feugiat quisque posuere velit quis. Sed volutpat et leo feugiat aenean eleifend praesent. Proin mauris vulputate adipiscing lacus consequat. Commodo neque malesuada rutrum ante. Ornare commodo eros, feugiat vitae potenti condimentum.</p>
                    </div>
                </div>
                <div class="col-md-4 text-right">
                    <div class="img-oppo2"><img src="{{ asset('images/appo2.png') }}" alt="" title=""/></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function() {

    });
</script>
@endsection