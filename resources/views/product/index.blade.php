@extends('layout')

@section('content')
    <div class="banner-global" style="background: url('images/banner-used-cars.jpg') no-repeat center;">
        <div class="css-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="{{ URL::to('/') }}">Home</a></li>
                    <li>/</li>
                    <li><a href="{{ URL::to('/product') }}">Products</a></li>
                </ul>
            </div>
        </div>
        <div class="tbl">
            <div class="cell">
                <div class="container">
                    <div class="title">Products</div>
                    <div class="bdy">
                        <p>Id risus est pellentesque tristique cras malesuada ante varius. Tempor, mi elementum consectetur ut ante dignissim pulvinar sit. Amet nibh massa quam maecenas in sit.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="css-product">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-lg-3">
                    <div class="row">
                        <div class="col-6 my-auto">
                            <div class="t-filters"><img src="{{ asset('images/filters.png') }}" alt="" title=""/> Filters</div>
                        </div>
                        <div class="col-6 my-auto text-end">
                            <div class="reset">Reset</div>
                        </div>
                    </div>
                    <div class="accordion-filter">
                        <div>
                            <div class="title-accordion">
                                <div class="in collapsed" data-bs-toggle="collapse" data-bs-target="#sec-1" aria-expanded="false" aria-controls="sec-1">Section 1<i class="fas fa-plus"></i></div>
                            </div>
                            <div id="sec-1" class="collapse">
                                <div class="pad">
                                    <ul class="css-radio">
                                        <li>
                                            <a>
                                                <input type="checkbox" id="product1-1" value="" name="section_1[]">
                                                <label for="product1-1">Product Category 1</label>
                                            </a>
                                        </li>
                                        <li>
                                            <a>
                                                <input type="checkbox" id="product1-2" value="" name="section_1[]">
                                                <label for="product1-2">Product Category 2</label>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>  
                        <div>
                            <div class="title-accordion">
                                <div class="in collapsed" data-bs-toggle="collapse" data-bs-target="#sec-2" aria-expanded="false">Section 2<i class="fas fa-plus"></i></div>
                            </div>
                            <div id="sec-2" class="collapse" style="">
                                <div class="pad">
                                    <ul class="css-radio">
                                        <li>
                                            <a>
                                                <input type="checkbox" id="product2-1" value="" name="section_2[]">
                                                <label for="product2-1">Product Category 1</label>
                                            </a>
                                        </li>
                                        <li>
                                            <a>
                                                <input type="checkbox" id="product2-2" value="" name="section_2[]">
                                                <label for="product2-2">Product Category 2</label>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="title-accordion">
                                <div class="in collapsed" data-bs-toggle="collapse" data-bs-target="#sec-3" aria-expanded="false">Section 3<i class="fas fa-plus"></i></div>
                            </div>
                            <div id="sec-3" class="collapse" style="">
                                <div class="pad">
                                    <ul class="css-radio">
                                        <li>
                                            <a>
                                                <input type="checkbox" id="product3-1" value="" name="section_2[]">
                                                <label for="product3-1">Product Category 1</label>
                                            </a>
                                        </li>
                                        <li>
                                            <a>
                                                <input type="checkbox" id="product3-2" value="" name="section_2[]">
                                                <label for="product3-2">Product Category 2</label>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="title-accordion">
                                <div class="in collapsed" data-bs-toggle="collapse" data-bs-target="#sec-4" aria-expanded="false">Section 4<i class="fas fa-plus"></i></div>
                            </div>
                            <div id="sec-4" class="collapse" style="">
                                <div class="pad">
                                    <ul class="css-radio">
                                        <li>
                                            <a>
                                                <input type="checkbox" id="product4-1" value="" name="section_2[]">
                                                <label for="product4-1">Product Category 1</label>
                                            </a>
                                        </li>
                                        <li>
                                            <a>
                                                <input type="checkbox" id="product4-2" value="" name="section_2[]">
                                                <label for="product4-2">Product Category 2</label>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-lg-9">
                    <ul class="nav nav-product" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="rims-tab" data-bs-toggle="pill" href="#rims" role="tab" aria-controls="rims" aria-selected="true">
                                Rims
                                <img class="img img1" src="{{ asset('images/rims.png') }}" alt="" title=""/>
                                <img class="img img2" src="{{ asset('images/rims2.png') }}" alt="" title=""/>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="accessories-tab" data-bs-toggle="pill" href="#accessories" role="tab" aria-controls="accessories" aria-selected="false">
                                Accessories
                                <img class="img img1" src="{{ asset('images/detailing.png') }}" alt="" title=""/>
                                <img class="img img2" src="{{ asset('images/detailing2.png') }}" alt="" title=""/>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="oil-tab" data-bs-toggle="pill" href="#oil" role="tab" aria-controls="oil" aria-selected="false">
                                Engine oil
                                <img class="img img1" src="{{ asset('images/oil.png') }}" alt="" title=""/>
                                <img class="img img2" src="{{ asset('images/oil2.png') }}" alt="" title=""/>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content tab-product">
                        <div class="tab-pane fade show active" id="rims" role="tabpanel" aria-labelledby="rims-tab">
                            <div class="row">
                                <div class="col-md-6 col-lg-4">
                                    <div class="item-product">
                                        <a href="{{ URL::to('/rims-detail') }}">
                                            <div class="img">
                                                <img src="{{ asset('images/product.jpg') }}" alt="" title=""/>
                                                <div class="merk">Indicator</div>
                                            </div>
                                            <div class="nm">Name Rims</div>
                                            <div class="price">$ 500</div>
                                            <div class="bdy">
                                                <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                            </div>
                                            <div class="txt">sub-detail</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <div class="item-product">
                                        <a href="{{ URL::to('/rims-detail') }}">
                                            <div class="img">
                                                <img src="{{ asset('images/product.jpg') }}" alt="" title=""/>
                                                <div class="merk">Indicator</div>
                                            </div>
                                            <div class="nm">Name Rims</div>
                                            <div class="price">$ 500</div>
                                            <div class="bdy">
                                                <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                            </div>
                                            <div class="txt">sub-detail</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <div class="item-product">
                                        <a href="{{ URL::to('/rims-detail') }}">
                                            <div class="img">
                                                <img src="{{ asset('images/product.jpg') }}" alt="" title=""/>
                                                <div class="merk">Indicator</div>
                                            </div>
                                            <div class="nm">Name Rims</div>
                                            <div class="price">$ 500</div>
                                            <div class="bdy">
                                                <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                            </div>
                                            <div class="txt">sub-detail</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <div class="item-product">
                                        <a href="{{ URL::to('/rims-detail') }}">
                                            <div class="img">
                                                <img src="{{ asset('images/product.jpg') }}" alt="" title=""/>
                                                <div class="merk">Indicator</div>
                                            </div>
                                            <div class="nm">Name Rims</div>
                                            <div class="price">$ 500</div>
                                            <div class="bdy">
                                                <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                            </div>
                                            <div class="txt">sub-detail</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <div class="item-product">
                                        <a href="{{ URL::to('/rims-detail') }}">
                                            <div class="img">
                                                <img src="{{ asset('images/product.jpg') }}" alt="" title=""/>
                                                <div class="merk">Indicator</div>
                                            </div>
                                            <div class="nm">Name Rims</div>
                                            <div class="price">$ 500</div>
                                            <div class="bdy">
                                                <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                            </div>
                                            <div class="txt">sub-detail</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <div class="item-product">
                                        <a href="{{ URL::to('/rims-detail') }}">
                                            <div class="img">
                                                <img src="{{ asset('images/product.jpg') }}" alt="" title=""/>
                                                <div class="merk">Indicator</div>
                                            </div>
                                            <div class="nm">Name Rims</div>
                                            <div class="price">$ 500</div>
                                            <div class="bdy">
                                                <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                            </div>
                                            <div class="txt">sub-detail</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <div class="item-product">
                                        <a href="{{ URL::to('/rims-detail') }}">
                                            <div class="img">
                                                <img src="{{ asset('images/product.jpg') }}" alt="" title=""/>
                                                <div class="merk">Indicator</div>
                                            </div>
                                            <div class="nm">Name Rims</div>
                                            <div class="price">$ 500</div>
                                            <div class="bdy">
                                                <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                            </div>
                                            <div class="txt">sub-detail</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <div class="item-product">
                                        <a href="{{ URL::to('/rims-detail') }}">
                                            <div class="img">
                                                <img src="{{ asset('images/product.jpg') }}" alt="" title=""/>
                                                <div class="merk">Indicator</div>
                                            </div>
                                            <div class="nm">Name Rims</div>
                                            <div class="price">$ 500</div>
                                            <div class="bdy">
                                                <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                            </div>
                                            <div class="txt">sub-detail</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <div class="item-product">
                                        <a href="{{ URL::to('/rims-detail') }}">
                                            <div class="img">
                                                <img src="{{ asset('images/product.jpg') }}" alt="" title=""/>
                                                <div class="merk">Indicator</div>
                                            </div>
                                            <div class="nm">Name Rims</div>
                                            <div class="price">$ 500</div>
                                            <div class="bdy">
                                                <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                            </div>
                                            <div class="txt">sub-detail</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="accessories" role="tabpanel" aria-labelledby="accessories-tab">
                            <div class="row">
                                <div class="col-md-6 col-lg-4">
                                    <div class="item-product">
                                        <a href="{{ URL::to('/accessories-detail') }}">
                                            <div class="img">
                                                <img src="{{ asset('images/product.jpg') }}" alt="" title=""/>
                                                <div class="merk">Indicator</div>
                                            </div>
                                            <div class="nm">Name Accessories</div>
                                            <div class="price">$ 500</div>
                                            <div class="bdy">
                                                <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                            </div>
                                            <div class="txt">sub-detail</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <div class="item-product">
                                        <a href="{{ URL::to('/accessories-detail') }}">
                                            <div class="img">
                                                <img src="{{ asset('images/product.jpg') }}" alt="" title=""/>
                                                <div class="merk">Indicator</div>
                                            </div>
                                            <div class="nm">Name Accessories</div>
                                            <div class="price">$ 500</div>
                                            <div class="bdy">
                                                <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                            </div>
                                            <div class="txt">sub-detail</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <div class="item-product">
                                        <a href="{{ URL::to('/accessories-detail') }}">
                                            <div class="img">
                                                <img src="{{ asset('images/product.jpg') }}" alt="" title=""/>
                                                <div class="merk">Indicator</div>
                                            </div>
                                            <div class="nm">Name Accessories</div>
                                            <div class="price">$ 500</div>
                                            <div class="bdy">
                                                <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                            </div>
                                            <div class="txt">sub-detail</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <div class="item-product">
                                        <a href="{{ URL::to('/accessories-detail') }}">
                                            <div class="img">
                                                <img src="{{ asset('images/product.jpg') }}" alt="" title=""/>
                                                <div class="merk">Indicator</div>
                                            </div>
                                            <div class="nm">Name Accessories</div>
                                            <div class="price">$ 500</div>
                                            <div class="bdy">
                                                <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                            </div>
                                            <div class="txt">sub-detail</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <div class="item-product">
                                        <a href="{{ URL::to('/accessories-detail') }}">
                                            <div class="img">
                                                <img src="{{ asset('images/product.jpg') }}" alt="" title=""/>
                                                <div class="merk">Indicator</div>
                                            </div>
                                            <div class="nm">Name Accessories</div>
                                            <div class="price">$ 500</div>
                                            <div class="bdy">
                                                <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                            </div>
                                            <div class="txt">sub-detail</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <div class="item-product">
                                        <a href="{{ URL::to('/accessories-detail') }}">
                                            <div class="img">
                                                <img src="{{ asset('images/product.jpg') }}" alt="" title=""/>
                                                <div class="merk">Indicator</div>
                                            </div>
                                            <div class="nm">Name Accessories</div>
                                            <div class="price">$ 500</div>
                                            <div class="bdy">
                                                <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                            </div>
                                            <div class="txt">sub-detail</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="oil" role="tabpanel" aria-labelledby="oil-tab">
                            <div class="row">
                                <div class="col-md-6 col-lg-4">
                                    <div class="item-product">
                                        <a href="{{ URL::to('/engine-detail') }}">
                                            <div class="img">
                                                <img src="{{ asset('images/product.jpg') }}" alt="" title=""/>
                                                <div class="merk">Indicator</div>
                                            </div>
                                            <div class="nm">Name Oils</div>
                                            <div class="price">$ 500</div>
                                            <div class="bdy">
                                                <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                            </div>
                                            <div class="txt">sub-detail</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <div class="item-product">
                                        <a href="{{ URL::to('/engine-detail') }}">
                                            <div class="img">
                                                <img src="{{ asset('images/product.jpg') }}" alt="" title=""/>
                                                <div class="merk">Indicator</div>
                                            </div>
                                            <div class="nm">Name Oils</div>
                                            <div class="price">$ 500</div>
                                            <div class="bdy">
                                                <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                            </div>
                                            <div class="txt">sub-detail</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <div class="item-product">
                                        <a href="{{ URL::to('/engine-detail') }}">
                                            <div class="img">
                                                <img src="{{ asset('images/product.jpg') }}" alt="" title=""/>
                                                <div class="merk">Indicator</div>
                                            </div>
                                            <div class="nm">Name Oils</div>
                                            <div class="price">$ 500</div>
                                            <div class="bdy">
                                                <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                            </div>
                                            <div class="txt">sub-detail</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <div class="item-product">
                                        <a href="{{ URL::to('/engine-detail') }}">
                                            <div class="img">
                                                <img src="{{ asset('images/product.jpg') }}" alt="" title=""/>
                                                <div class="merk">Indicator</div>
                                            </div>
                                            <div class="nm">Name Oils</div>
                                            <div class="price">$ 500</div>
                                            <div class="bdy">
                                                <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                            </div>
                                            <div class="txt">sub-detail</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <div class="item-product">
                                        <a href="{{ URL::to('/engine-detail') }}">
                                            <div class="img">
                                                <img src="{{ asset('images/product.jpg') }}" alt="" title=""/>
                                                <div class="merk">Indicator</div>
                                            </div>
                                            <div class="nm">Name Oils</div>
                                            <div class="price">$ 500</div>
                                            <div class="bdy">
                                                <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                            </div>
                                            <div class="txt">sub-detail</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4">
                                    <div class="item-product">
                                        <a href="{{ URL::to('/engine-detail') }}">
                                            <div class="img">
                                                <img src="{{ asset('images/product.jpg') }}" alt="" title=""/>
                                                <div class="merk">Indicator</div>
                                            </div>
                                            <div class="nm">Name Oils</div>
                                            <div class="price">$ 500</div>
                                            <div class="bdy">
                                                <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                            </div>
                                            <div class="txt">sub-detail</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="l-pagination">
                        <li><a>Previous</a></li>
                        <li class="mr5">/</li>
                        <li><a class="active">1</a></li>
                        <li><a>2</a></li>
                        <li><a>3</a></li>
                        <li><a>4</a></li>
                        <li><a>...</a></li>
                        <li class="mr5">/</li>
                        <li><a>Next</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
<script type="text/javascript">
	$(document).ready(function() {
        $('.nav-products').addClass('active');

        $(".collapse.show").each(function(){
            $(this).prev(".title-accordion").find(".fas").addClass("fa-minus").removeClass("fa-plus");
        });
        
        $(".collapse").on('show.bs.collapse', function(){
            $(this).prev(".title-accordion").find(".fas").removeClass("fa-plus").addClass("fa-minus");
        }).on('hide.bs.collapse', function(){
            $(this).prev(".title-accordion").find(".fas").removeClass("fa-minus").addClass("fa-plus");
        });

        $('.reset').click(function(event) {
            $('.collapse').removeClass("show");
            $(".title-accordion").find(".fas").addClass("fa-plus").removeClass("fa-minus");
            $('.css-radio input').prop('checked', false);
        });
	});
</script>
@endsection