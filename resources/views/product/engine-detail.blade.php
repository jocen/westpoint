@extends('layout')

@section('content')
    <div class="css-product css-detail">
        <div class="container">
            <ul class="css-breadcrumb">
                <li><a href="{{ URL::to('/') }}">Home</a></li>
                <li>/</li>
                <li><a href="{{ URL::to('/product') }}">Products</a></li>
                <li>/</li>
                <li><a href="{{ URL::to('/engine-detail') }}">Product Name</a></li>
            </ul>
            <div class="row row5">
                <div class="col-md-1 order-2 order-md-1">
                    <div class="slider-thumb-product">
                        <div class="item">
                            <img src="{{ asset('images/product-detail.jpg') }}" alt="" title=""/>
                        </div>
                        <div class="item">
                            <img src="{{ asset('images/product-detail.jpg') }}" alt="" title=""/>
                        </div>
                        <div class="item">
                            <img src="{{ asset('images/product-detail.jpg') }}" alt="" title=""/>
                        </div>
                        <div class="item">
                            <img src="{{ asset('images/product-detail.jpg') }}" alt="" title=""/>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 order-1 order-md-2">
                    <div class="slider-product">
                        <div class="item">
                            <img src="{{ asset('images/product-detail.jpg') }}" alt="" title=""/>
                        </div>
                        <div class="item">
                            <img src="{{ asset('images/product-detail.jpg') }}" alt="" title=""/>
                        </div>
                        <div class="item">
                            <img src="{{ asset('images/product-detail.jpg') }}" alt="" title=""/>
                        </div>
                        <div class="item">
                            <img src="{{ asset('images/product-detail.jpg') }}" alt="" title=""/>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 order-3">
                    <div class="pl30">
                        <div class="box-category">Engine Oil</div>
                        <div class="nm-detail">SHELL Helix Car Engine Oil</div>
                        <div class="price-detail">S$ 500</div>
                        <div class="desc-detail">
                            <div class="mb20">
                                <div class="tbl">
                                    <div class="cell w100">Brand:</div>
                                    <div class="cell">SHELL</div>
                                </div>
                            </div>
                            <div class="mb20">
                                <div class="tbl">
                                    <div class="cell w100">Grade:</div>
                                    <div class="cell">A</div>
                                </div>
                            </div>
                            <div>
                                <div class="tbl">
                                    <div class="cell w100">Litres:</div>
                                    <div class="cell">5 L</div>
                                </div>
                            </div>
                        </div>
                        <ul class="l-btn">
                            <li><a href="{{ URL::to('/appointment') }}"><button class="hvr-button" type="button">Reserve now</button></a></li>
                            <li><a href="#"><button class="hvr-button" type="button">Chat with us</button></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <ul class="nav nav-product-detail" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="details-tab" data-bs-toggle="pill" href="#details" role="tab" aria-controls="details" aria-selected="true">Details</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="product-information-tab" data-bs-toggle="pill" href="#product-information" role="tab" aria-controls="product-information" aria-selected="false">Product information</a>
                </li>
            </ul>
            <div class="tab-content tab-product-detail">
                <div class="tab-pane fade show active" id="details" role="tabpanel" aria-labelledby="details-tab">
                    <div class="bdy">
                        <p>Montes, aenean ut magna tempus amet, non eu tincidunt morbi. Lacus, rhoncus, feugiat quisque posuere velit quis. Sed volutpat et leo feugiat aenean eleifend praesent. Proin mauris vulputate adipiscing lacus consequat. Commodo neque malesuada rutrum ante.</p><br/>
                        <p>Ornare commodo eros, feugiat vitae potenti condimentum. Felis pretium, varius vel erat ut. Enim pellentesque lacus, iaculis maecenas volutpat nunc elementum tempus vel. Hendrerit eget ornare in suspendisse varius sed. Et suspendisse lacinia morbi vestibulum elementum phasellus. Enim scelerisque ut nulla donec nulla. </p>
                    </div>
                </div>            
                <div class="tab-pane fade" id="product-information" role="tabpanel" aria-labelledby="product-information-tab">
                    <div class="bdy">
                        <p>Montes, aenean ut magna tempus amet, non eu tincidunt morbi. Lacus, rhoncus, feugiat quisque posuere velit quis. Sed volutpat et leo feugiat aenean eleifend praesent. Proin mauris vulputate adipiscing lacus consequat. Commodo neque malesuada rutrum ante.</p><br/>
                        <p>Ornare commodo eros, feugiat vitae potenti condimentum. Felis pretium, varius vel erat ut. Enim pellentesque lacus, iaculis maecenas volutpat nunc elementum tempus vel. Hendrerit eget ornare in suspendisse varius sed. Et suspendisse lacinia morbi vestibulum elementum phasellus. Enim scelerisque ut nulla donec nulla. </p>
                    </div>
                </div>
            </div>
            <div class="t-product">You may also like</div>
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="slider-related">
                        <div class="item-product">
                            <a href="{{ URL::to('/engine-detail') }}">
                                <div class="img">
                                    <img src="{{ asset('images/product.jpg') }}" alt="" title=""/>
                                    <div class="merk">Indicator</div>
                                </div>
                                <div class="nm">Name Accessories</div>
                                <div class="price">$ 500</div>
                                <div class="bdy">
                                    <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                </div>
                                <div class="txt">sub-detail</div>
                            </a>
                        </div>
                        <div class="item-product">
                            <a href="{{ URL::to('/engine-detail') }}">
                                <div class="img">
                                    <img src="{{ asset('images/product.jpg') }}" alt="" title=""/>
                                </div>
                                <div class="nm">Name Accessories</div>
                                <div class="price">$ 500</div>
                                <div class="bdy">
                                    <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                </div>
                                <div class="txt">sub-detail</div>
                            </a>
                        </div>
                        <div class="item-product">
                            <a href="{{ URL::to('/engine-detail') }}">
                                <div class="img">
                                    <img src="{{ asset('images/product.jpg') }}" alt="" title=""/>
                                </div>
                                <div class="nm">Name Accessories</div>
                                <div class="price">$ 500</div>
                                <div class="bdy">
                                    <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                </div>
                                <div class="txt">sub-detail</div>
                            </a>
                        </div>
                        <div class="item-product">
                            <a href="{{ URL::to('/engine-detail') }}">
                                <div class="img">
                                    <img src="{{ asset('images/product.jpg') }}" alt="" title=""/>
                                    <div class="merk">Indicator</div>
                                </div>
                                <div class="nm">Name Accessories</div>
                                <div class="price">$ 500</div>
                                <div class="bdy">
                                    <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                                </div>
                                <div class="txt">sub-detail</div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')

<script type="text/javascript">
	$(document).ready(function() {
        $('.slider-product').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            fade: false,
            asNavFor: '.slider-thumb-product',
            infinite: false
        });

        $('.slider-thumb-product').slick({
            // autoplay: false,
            // arrows: false,
            // dots: false,
            // slidesToShow: 3,
            // centerPadding: "10px",
            // draggable: false,
            // infinite: true,
            // pauseOnHover: false,
            // swipe: false,
            // touchMove: false,
            // vertical: true,
            // speed: 1000,
            // autoplaySpeed: 2000,
            // useTransform: true,
            vertical: true,
            verticalSwiping:true,
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: '.slider-product',
            dots: false,
            arrows: false,
            centerMode: false,
            focusOnSelect: true,
            infinite: false,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3,
                        vertical: false,
                        arrows: true
                    }
                }
            ]
        });

        $('.slider-related').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            centerMode: false,
            focusOnSelect: true,
            infinite: false,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                    }
                }
            ]
        });

        $('.nav-products').addClass('active');
	});
</script>
@endsection