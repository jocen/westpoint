<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=2.0"/>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <title>WEST POINT</title>
    <!--favicon-->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/favicon-180x180.png?v.2') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicon-32x32.png?v.2') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon-16x16.png?v.2') }}">
    
    <!-- CSS -->
    @yield('css')
    <link href="{{ asset('js/jquery-ui/jquery-ui.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/fonts.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/front.css') }}" rel="stylesheet"/>

</head>
<body>

<section id="main-page">
    <div class="bg-dark-menu"></div>
    
    <header>
        <div class="container">
            <div class="row">
                <div class="col-9 my-auto">
                    <div class="menu">
                        <i class="fas fa-bars"></i>
                    </div>
                    <div class="logo">
                        <a href="{{ URL::to('/') }}"><img src="{{ asset('images/logo.png') }}" alt="" title=""/></a>
                    </div>
                    <ul class="main-menu">
                        <li><a class="nav-home" href="{{ URL::to('/') }}">Home</a></li>
                        <li><a class="nav-about" href="{{ URL::to('/about-us') }}">About Us</a></li>
                        <li><a class="nav-gallery" href="{{ URL::to('/gallery-reviews') }}">Gallery & Reviews</a></li>
                        <li><a class="nav-services" href="{{ URL::to('/services') }}">Services</a></li>
                        <li><a class="nav-products" href="{{ URL::to('/product') }}">Products</a></li>
                        <li><a class="nav-buy" href="{{ URL::to('/used-cars') }}">Buy & Sell Cars</a></li>
                    </ul>
                </div>
                <div class="col-3 my-auto text-end">
                    <!-- NO HAVE ACCOUNT -->
                    <ul class="l-login css-no-account">
                        <li class="nav-login"><a href="{{ URL::to('/login') }}">Login</a></li>
                        <li class="register"><a href="{{ URL::to('/register') }}">Sign Up</a></li>
                        <li class="mobile-login">
                            <a href="{{ URL::to('/login') }}"><i class="fas fa-sign-in-alt"></i></a>
                        </li>
                    </ul>

                    <!-- HAVE ACCOUNT -->
                    <ul class="l-login css-account">
                        <li><a href="{{ URL::to('/') }}">Signout</a></li>
                        <li class="account"><a href="{{ URL::to('/regular/dashboard') }}"><img src="{{ asset('images/account.png') }}" alt="" title=""/> My Account</a></li>
                        <li><a href="{{ URL::to('/regular/dashboard') }}"><i class="fas fa-user-circle"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </header>

    <div id="main">
        @yield('content')
    </div>

    <footer id="footer">
        <div class="container">
            <div class="logo">
                <a href="{{ URL::to('/') }}"><img src="{{ asset('images/logo.png') }}" alt="" title=""/></a>
            </div>
            <div class="row">
                <div class="col-lg-10">
                    <ul class="l-footer">
                        <li>
                            <div class="t1">Opening Hours:</div>
                            <div class="t2"><p>Mon - Fri | 10am - 7pm GMT +8</p></div>
                        </li>
                        <li>
                            <div class="t1">Address:</div>
                            <div class="t2">
                                <p>123 Chai Chee Road,</p>
                                <p>Singapore 560789</p>
                            </div>
                        </li>
                        <li>
                            <div class="t2 m5"><p>All Rights Reserved <?php echo date('Y'); ?> Wespoint</p></div>
                            <div class="t3"><a href="{{ URL::to('/terms') }}">Terms & Conditions</a></div>
                            <div class="t3"><a href="{{ URL::to('/privacy-policy') }}">Privacy Policy</a></div>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-2 text-end">
                    <div class="t4">Connect with Us:</div>
                    <ul class="l-soc">
                        <li><a href="#" target="_blank" rel="noreferrer noopener">  <i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#" target="_blank" rel="noreferrer noopener"><i class="fab fa-linkedin-in"></i></a></li>
                        <li><a href="#" target="_blank" rel="noreferrer noopener"><i class="fab fa-instagram"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
</section>

<div class="box-wa">
    <ul class="l-wa">
        <li class="bg-wa"><a href="#" target="_blank"><div class="bg-chat">Need Help? Chat with us</div> <div class="bg-wa"><i class="fab fa-whatsapp"></i></div></a></li>
        <li class="bg-fb"><a href="#" target="_blank"><i class="fa-brands fa-facebook"></i></a></li>
    </ul>
</div>

<div id="menu">
    <div class="close-menu">
        <img src="{{asset('images/close-menu.png')}}" title="" alt=""/>
    </div>
    <div class="l-menu">
        <ul>
            <li><a class="nav-home" href="{{ URL::to('/') }}">Home</a></li>
            <li><a class="nav-about" href="{{ URL::to('/about-us') }}">About</a></li>
            <li><a class="nav-gallery" href="{{ URL::to('/gallery-reviews') }}">Gallery & Reviews</a></li>
            <li><a class="nav-services" href="{{ URL::to('/services') }}">Services</a></li>
            <li><a class="nav-products" href="{{ URL::to('/product') }}">Products</a></li>
            <li><a class="nav-buy" href="{{ URL::to('/used-cars') }}">Buy & Sell Cars</a></li>
        </ul>
    </div>
</div>
   

<script type="text/javascript" src="{{ asset('/js/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery-ui/jquery-ui.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.ui.touch-punch.min.js') }}"></script>

<!-- JS -->
@yield('js')
<script type="text/javascript">
    function windowWidth() {
        if ($(window).width() > 992) {
            $(window).scroll(function () {
                if (
                    $(window).scrollTop() + $(window).height() <
                    $(document).height() - $("#footer").height()
                ) {
                    $(".box-wa").css("bottom", "30px");
                }
                if (
                    $(window).scrollTop() + $(window).height() >
                    $(document).height() - $("#footer").height()
                ) {
                    $(".box-wa").css("bottom", "100px");
                }
            });
        }
    }

    function scrollAll() {
        a = $(window).scrollTop();
        if (a > 200) {
            $('.box-wa').fadeIn();
        } else {
            $('.box-wa').fadeOut();
        }
    }
    $(document).ready(function() {
        scrollAll();

        $(window).scroll(function() {
            scrollAll();
        });

        windowWidth();
        
        $(window).on('resize', function() {
            windowWidth();
        });

        $(".only-number").keydown(function (e) {
            if (
                $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                (e.keyCode >= 35 && e.keyCode <= 39)
            ) {
                return;
            }
            if (
                (e.shiftKey || e.keyCode < 48 || e.keyCode > 57) &&
                (e.keyCode < 96 || e.keyCode > 105)
            ) {
                e.preventDefault();
            }
        });

        $(".menu").click(function(a) {
            a.preventDefault();
            $('#menu').addClass('open');
            $("body").addClass('no-scroll');
            $(".bg-dark-menu").show();
            $(".bg-dark-menu").animate({
                opacity: .5
            });
        });

        $("html").click(function(a) {
            if (!$(a.target).parents().is(".menu") && !$(a.target).is("#menu") && !$(a.target).is(".close-menu") && !$(a.target).parents().is("#menu")) {
                $('#menu').removeClass('open');
                $("body").removeClass('no-scroll');
                $(".bg-dark-menu").hide();
                $(".bg-dark-menu").animate({
                    opacity: 0
                });
            }
        });

        $(".close-menu").click(function(a) {
            $('#menu').removeClass('open');
            $("body").removeClass('no-scroll');
            $(".bg-dark-menu").hide();
            $(".bg-dark-menu").animate({
                opacity: 0
            });
        });

        $(document).keyup(function(a) {
            if (27 == a.keyCode) {
                $('#menu').removeClass('open');
                $("body").removeClass('no-scroll');
                $(".bg-dark-menu").hide();
                $(".bg-dark-menu").animate({
                    opacity: 0
                });
            }
        });
    }); 
</script>

</body>
</html>
        
