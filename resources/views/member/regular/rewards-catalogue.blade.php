@extends('layout')

@section('content')
    <div class="banner-global" style="background: url('../images/banner-account.jpg') no-repeat center;">
        <div class="css-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="{{ URL::to('/') }}">Home</a></li>
                    <li>/</li>
                    <li><a href="{{ URL::to('/regular/dashboard') }}">My Account</a></li>
                </ul>
            </div>
        </div>
        <div class="tbl">
            <div class="cell">
                <div class="container">
                    <div class="title">My Account</div>
                </div>
            </div>
        </div>
    </div>
    <div class="css-account">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="left-account">
                        @include('member.regular.menu')
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="right-account">
                        <div class="title-account mb20">Rewards Catalogue</div>
                        <div class="bdy-account">
                            <p>We will send you an email with a QR code for redemption after you have redeemed a reward with your available points. To use your reward, please flash the QR code in the email when you are at our office. </p>
                        </div>
                        <div class="row" id="myScroll">
                            <div class="col-6 col-md-4 col-lg-3">
                                <div class="item-catalogue">
                                    <a data-bs-toggle="modal" data-bs-target="#modal-rewards-unused">
                                        <div class="img"><img src="{{ asset('images/reward1.jpg') }}" alt="" title=""/></div>
                                        <div class="t1">FREE Basic Servicing (Castrol)</div>
                                        <div class="t2">1,000 points</div>
                                    </a>
                                    <div class="link-reward">
                                        <button class="hvr-button" type="button" data-bs-toggle="modal" data-bs-target="#modal-redeem">Redeem</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 col-md-4 col-lg-3">
                                <div class="item-catalogue">
                                    <a data-bs-toggle="modal" data-bs-target="#modal-rewards-unused">
                                        <div class="img"><img src="{{ asset('images/reward1.jpg') }}" alt="" title=""/></div>
                                        <div class="t1">FREE Basic Servicing (Castrol)</div>
                                        <div class="t2">1,000 points</div>
                                    </a>
                                    <div class="link-reward">
                                        <button class="hvr-button" type="button" data-bs-toggle="modal" data-bs-target="#modal-redeem">Redeem</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 col-md-4 col-lg-3">
                                <div class="item-catalogue">
                                    <a data-bs-toggle="modal" data-bs-target="#modal-rewards-unused">
                                        <div class="img"><img src="{{ asset('images/reward1.jpg') }}" alt="" title=""/></div>
                                        <div class="t1">FREE Basic Servicing (Castrol)</div>
                                        <div class="t2">1,000 points</div>
                                    </a>
                                    <div class="link-reward">
                                        <button class="hvr-button" type="button" data-bs-toggle="modal" data-bs-target="#modal-redeem">Redeem</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 col-md-4 col-lg-3">
                                <div class="item-catalogue">
                                    <a data-bs-toggle="modal" data-bs-target="#modal-rewards-unused">
                                        <div class="img"><img src="{{ asset('images/reward1.jpg') }}" alt="" title=""/></div>
                                        <div class="t1">FREE Basic Servicing (Castrol)</div>
                                        <div class="t2">1,000 points</div>
                                    </a>
                                    <div class="link-reward">
                                        <button class="hvr-button" type="button" data-bs-toggle="modal" data-bs-target="#modal-redeem">Redeem</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 col-md-4 col-lg-3">
                                <div class="item-catalogue">
                                    <a data-bs-toggle="modal" data-bs-target="#modal-rewards-unused">
                                        <div class="img"><img src="{{ asset('images/reward1.jpg') }}" alt="" title=""/></div>
                                        <div class="t1">FREE Basic Servicing (Castrol)</div>
                                        <div class="t2">1,000 points</div>
                                    </a>
                                    <div class="link-reward">
                                        <button class="hvr-button" type="button" data-bs-toggle="modal" data-bs-target="#modal-redeem">Redeem</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 col-md-4 col-lg-3">
                                <div class="item-catalogue">
                                    <a data-bs-toggle="modal" data-bs-target="#modal-rewards-unused">
                                        <div class="img"><img src="{{ asset('images/reward1.jpg') }}" alt="" title=""/></div>
                                        <div class="t1">FREE Basic Servicing (Castrol)</div>
                                        <div class="t2">1,000 points</div>
                                    </a>
                                    <div class="link-reward">
                                        <button class="hvr-button" type="button" data-bs-toggle="modal" data-bs-target="#modal-redeem">Redeem</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 col-md-4 col-lg-3">
                                <div class="item-catalogue">
                                    <a data-bs-toggle="modal" data-bs-target="#modal-rewards-unused">
                                        <div class="img"><img src="{{ asset('images/reward1.jpg') }}" alt="" title=""/></div>
                                        <div class="t1">FREE Basic Servicing (Castrol)</div>
                                        <div class="t2">1,000 points</div>
                                    </a>
                                    <div class="link-reward">
                                        <button class="hvr-button" type="button" data-bs-toggle="modal" data-bs-target="#modal-redeem">Redeem</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 col-md-4 col-lg-3">
                                <div class="item-catalogue">
                                    <a data-bs-toggle="modal" data-bs-target="#modal-rewards-unused">
                                        <div class="img"><img src="{{ asset('images/reward1.jpg') }}" alt="" title=""/></div>
                                        <div class="t1">FREE Basic Servicing (Castrol)</div>
                                        <div class="t2">1,000 points</div>
                                    </a>
                                    <div class="link-reward">
                                        <button class="hvr-button" type="button" data-bs-toggle="modal" data-bs-target="#modal-redeem">Redeem</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="link-load">
                            <a><button type="button" class="hvr-button black"><i class="fas fa-redo" aria-hidden="true"></i> Load More</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-redeem" class="modal fade modal-global" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="pad-pop">
                    <div class="close-pop" data-bs-dismiss="modal">
                        <img src="{{ asset('images/close.png') }}" alt="" title=""/>
                    </div>
                    <div class="pad-bdy">
                        <div class="img-success">
                            <img src="{{ asset('images/pop-reward.png') }}" alt="" title=""/>
                        </div>
                        <div class="t-pop">Reward Redeemed!</div>
                        <div class="t-pop2">
                            <p>Montes, aenean ut magna tempus amet, non eu tincidunt morbi. Lacus, rhoncus, feugiat quisque posuere velit quis.</p><br/>
                            <p>Sed volutpat et leo feugiat aenean eleifend praesent.</p>
                        </div>
                        <div class="btn-pop">
                            <a data-bs-dismiss="modal">
                                <button class="hvr-button" type="button">Ok, Got it</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-rewards-unused" class="modal fade modal-global modal-rewards" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="img-pop"><img src="{{ asset('images/rewards.jpg') }}" alt="" title=""/></div>
                <div class="pad-pop">
                    <div class="pad-bdy">
                        <div class="t-pop">Free Car Wash and Vacuum</div>
                        <div class="text-center">
                            <div class="box-catalogue">
                                <img src="{{ asset('images/catalogue-point.png') }}" alt="" title=""/> 1,000 points
                            </div>
                        </div>
                        <div class="bdr-pop"></div>
                        <div class="t-pop2 mb40">
                            <div class="mb25">
                                <p>Montes, aenean ut magna tempus amet, non eu tincidunt morbi. Lacus, rhoncus, feugiat quisque posuere velit quis. Sed volutpat et leo feugiat aenean eleifend praesent. </p>
                            </div>
                            <div class="mb25">
                                <p>Voucher is auto-credited to your account. To redeem the voucher, follow the steps:</p>
                                <p>1 ) Book a Service with us</p>
                                <p>2 ) Flash the QR code you received from us via email</p>
                                <p>3 ) Your redemption is done!</p>
                            </div>
                            <div>
                                <p class="black">Terms and Conditions</p>
                                <ul>
                                    <li>Montes, aenean ut magna tempus amet, non eu tincidunt morbi.</li>
                                    <li>Lacus, rhoncus, feugiat quisque posuere velit quis.</li>
                                    <li>Sed volutpat et leo feugiat aenean eleifend praesent.</li>
                                    <li>Montes, aenean ut magna tempus amet, non eu tincidunt morbi.</li>
                                    <li>Lacus, rhoncus, feugiat quisque posuere velit quis.</li>
                                    <li>Sed volutpat et leo feugiat aenean eleifend praesent.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="btn-pop">
                            <a data-bs-dismiss="modal">
                                <button class="hvr-button" type="button">Close</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

<script type="text/javascript">
	$(document).ready(function() {
		$('.nav-catalogue').addClass('active');

        $('ul.l-account-resp li a').click(function(event) {
            $('ul.l-account').toggleClass('open');
        });

        $('.css-no-account').addClass('active');
        
        $('.css-account').addClass('active');

        $('header').addClass('account');

        var counter=0;
        $(window).scroll(function () {
            if ($(window).scrollTop() == $(document).height() - $(window).height() && counter < 2) {
                appendData();
            }
        });
        function appendData() {
            var html = '';
            for (i = 0; i < 10; i++) {
                html += '<div class="col-6 col-md-4 col-lg-3"><div class="item-catalogue"><a data-bs-toggle="modal" data-bs-target="#modal-rewards-unused"><div class="img"><img src="{{ asset('images/reward1.jpg') }}" alt="" title=""/></div><div class="t1">FREE Basic Servicing (Castrol)</div><div class="t2">1,000 points</div></a><div class="link-reward"><button class="hvr-button" type="button" data-bs-toggle="modal" data-bs-target="#modal-redeem">Redeem</button></div></div></div>';
            }
            $('#myScroll').append(html);
            counter++;
            $('.link-load').hide();
        }
	});
</script>
@endsection