@extends('layout')

@section('content')
    <div class="banner-global" style="background: url('../images/banner-account.jpg') no-repeat center;">
        <div class="css-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="{{ URL::to('/') }}">Home</a></li>
                    <li>/</li>
                    <li><a href="{{ URL::to('/regular/dashboard') }}">My Account</a></li>
                </ul>
            </div>
        </div>
        <div class="tbl">
            <div class="cell">
                <div class="container">
                    <div class="title">My Account</div>
                </div>
            </div>
        </div>
    </div>
    <div class="css-account">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="left-account">
                        @include('member.regular.menu')
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="right-account">
                        <div class="row">
                            <div class="col-md-11 col-lg-9">
                                <form class="css-form">
                                    <div class="mb70">
                                        <div class="title-account mb20">Personal information</div>
                                        <div class="form-group">
                                            <label id="name">Name</label>
                                            <input class="form-control" id="name" name="name" value="David Lim" type="text" required="" />
                                        </div> 
                                        <div class="form-group">
                                            <label id="email">Email</label>
                                            <input class="form-control" id="email" name="email" type="text" value="davidlim@gmail.com" required="" />
                                        </div>  
                                        <div class="form-group">
                                            <label id="phone">Contact Number</label>
                                            <input class="form-control only-number" id="phone" name="phone" type="text" value="9008 7665" required="" />
                                        </div>
                                        <div class="form-group mb0">
                                            <label id="password">Password</label>
                                            <input class="form-control only-number" id="password" name="password" type="password" value="" required="" />
                                        </div>
                                    </div>
                                    <div>
                                        <div class="title-account mb20">Vehicle Information</div>
                                        <div class="form-group">
                                            <label id="car-plate">Car Plate Number</label>
                                            <input class="form-control" id="car-plate" name="car-plate" value="SLE 9880Z" type="text" required="" />
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6"> 
                                                <div class="form-group">
                                                    <label id="car-make">Car Make</label>
                                                    <input class="form-control" id="car-make" name="car-make" type="text" value="Toyota" required="" />
                                                </div>  
                                            </div>
                                            <div class="col-md-6"> 
                                                <div class="form-group">
                                                    <label id="car-model">Car Model</label>
                                                    <input class="form-control" id="car-model" name="car-model" type="text" value="Camry" required="" />
                                                </div>  
                                            </div>
                                        </div>
                                    </div>
                                    <div class="button-account">
                                        <button class="hvr-button" type="submit">Save Changes</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

<script type="text/javascript">
	$(document).ready(function() {
        $('.nav-info').addClass('active');
        
        $('header').addClass('account');

        $('ul.l-account-resp li a').click(function(event) {
            $('ul.l-account').toggleClass('open');
        });
	});
</script>
@endsection