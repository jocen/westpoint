@extends('layout')

@section('content')
    <div class="banner-global" style="background: url('../images/banner-account.jpg') no-repeat center;">
        <div class="css-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="{{ URL::to('/') }}">Home</a></li>
                    <li>/</li>
                    <li><a href="{{ URL::to('/regular/dashboard') }}">My Account</a></li>
                </ul>
            </div>
        </div>
        <div class="tbl">
            <div class="cell">
                <div class="container">
                    <div class="title">My Account</div>
                </div>
            </div>
        </div>
    </div>
    <div class="css-account">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="left-account">
                        @include('member.regular.menu')
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="right-account">
                        <div class="box-account">
                            <div class="img-profile"><img src="{{ asset('images/profile.png') }}" alt="" title=""/></div>
                            <div class="name">Welcome back David!</div>
                            <div class="bdy">
                                <p>Montes, aenean ut magna tempus amet, non eu tincidunt morbi. Lacus, rhoncus, feugiat quisque posuere velit quis. Sed volutpat et leo feugiat aenean eleifend praesent.</p>
                            </div>
                        </div>
                        <div class="mb70">
                            <div class="hide-xs">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="title-account">Personal details</div>
                                        <div class="link"><a href="{{ URL::to('/regular/account-information') }}">Edit</a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 my-auto order-2 order-md-1">
                                    <div class="show-xs">
                                        <div class="title-account">Personal details</div>
                                    </div>
                                    <form class="css-form">
                                        <div class="form-group">
                                            <label id="name">Name</label>
                                            <input class="form-control" id="name" name="name" value="David Lim" type="text" readonly="" />
                                        </div> 
                                        <div class="form-group">
                                            <label id="email">Email</label>
                                            <input class="form-control" id="email" name="email" type="text" value="davidlim@gmail.com" readonly="" />
                                        </div>  
                                        <div class="form-group mb0">
                                            <label id="phone">Contact Number</label>
                                            <input class="form-control only-number" id="phone" name="phone" type="text" value="9008 7665" readonly="" />
                                        </div>
                                    </form>
                                    <div class="show-xs">
                                        <div class="btn-edit">
                                            <a href="{{ URL::to('/regular/account-information') }}">
                                                <button type="button" class="hvr-button">Edit</button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 my-auto order-1 order-md-2">
                                    <div class="box-point mt30">
                                        <div class="row">
                                            <div class="col-9 my-auto">
                                                <div class="t1">10,000</div>
                                                <div class="t2">Available Points</div>
                                            </div>
                                            <div class="col-3 my-auto text-end">
                                                <div class="img"><img src="{{ asset('images/point.png') }}" alt="" title=""/></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-point">
                                        <div class="row">
                                            <div class="col-9 my-auto">
                                                <div class="t1">3</div>
                                                <div class="t2">Unused Rewards</div>
                                            </div>
                                            <div class="col-3 my-auto text-end">
                                                <div class="img"><img src="{{ asset('images/my-reward.png') }}" alt="" title=""/></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-point mb0">
                                        <div class="row">
                                            <div class="col-9 my-auto">
                                                <div class="t1">1</div>
                                                <div class="t2">Upcoming Appointment</div>
                                            </div>
                                            <div class="col-3 my-auto text-end">
                                                <div class="img"><img src="{{ asset('images/calender.png') }}" alt="" title=""/></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="title-account mb20">My rewards</div>
                                    <div class="box-reward">
                                        <a data-bs-toggle="modal" data-bs-target="#modal-rewards-awarded">
                                            <div class="tbl">
                                                <div class="cell img-reward">
                                                    <img src="{{ asset('images/reward.jpg') }}" alt="" title=""/>
                                                </div>
                                                <div class="cell">
                                                    <div class="status-reward green">AWARDED</div>
                                                    <div class="t">FREE Basic Servicing (Castrol)</div>
                                                    <div class="time">Valid until 15 Feb 2022</div>
                                                </div>
                                            </div>
                                            <div class="view">View</div>
                                        </a>
                                    </div>
                                    <div class="box-reward">
                                        <a data-bs-toggle="modal" data-bs-target="#modal-rewards-awarded">
                                            <div class="tbl">
                                                <div class="cell img-reward">
                                                    <img src="{{ asset('images/reward.jpg') }}" alt="" title=""/>
                                                </div>
                                                <div class="cell">
                                                    <div class="status-reward green">AWARDED</div>
                                                    <div class="t">FREE Basic Servicing (Castrol)</div>
                                                    <div class="time">Valid until 15 Feb 2022</div>
                                                </div>
                                            </div>
                                            <div class="view">View</div>
                                        </a>
                                    </div>
                                    <div class="box-reward">
                                        <a data-bs-toggle="modal" data-bs-target="#modal-rewards-expired">
                                            <div class="tbl">
                                                <div class="cell img-reward">
                                                    <img src="{{ asset('images/reward.jpg') }}" alt="" title=""/>
                                                </div>
                                                <div class="cell">
                                                    <div class="status-reward grey">Expired</div>
                                                    <div class="t">FREE Basic Servicing (Castrol)</div>
                                                    <div class="time">Valid until 15 Feb 2022</div>
                                                </div>
                                            </div>
                                            <div class="view">View</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="title-account mb20 mt30">Upcoming appointments</div>
                                    <div class="box-appo">
                                        <div class="tbl">
                                            <div class="cell wdate">
                                                <div class="in">
                                                    <div class="date">25</div>
                                                    <div class="day">Mon</div>
                                                </div>
                                            </div>
                                            <div class="cell">
                                                <div class="t">Car rim replacement</div>
                                                <div class="time">10:00 am</div>
                                            </div>
                                        </div>
                                        <div class="view">
                                            <a href="{{ URL::to('/regular/my-bookings') }}">View</a>
                                        </div>
                                    </div>
                                    <div class="box-appo">
                                        <div class="tbl">
                                            <div class="cell wdate">
                                                <div class="in">
                                                    <div class="date">25</div>
                                                    <div class="day">Mon</div>
                                                </div>
                                            </div>
                                            <div class="cell">
                                                <div class="t">Car rim replacement</div>
                                                <div class="time">10:00 am</div>
                                            </div>
                                        </div>
                                        <div class="view">
                                            <a href="{{ URL::to('/regular/my-bookings') }}">View</a>
                                        </div>
                                    </div>
                                    <div class="box-appo">
                                        <div class="tbl">
                                            <div class="cell wdate">
                                                <div class="in">
                                                    <div class="date">25</div>
                                                    <div class="day">Mon</div>
                                                </div>
                                            </div>
                                            <div class="cell">
                                                <div class="t">Car rim replacement</div>
                                                <div class="time">10:00 am</div>
                                            </div>
                                        </div>
                                        <div class="view">
                                            <a href="{{ URL::to('/regular/my-bookings') }}">View</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-rewards-awarded" class="modal fade modal-global modal-rewards" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="img-pop"><img src="{{ asset('images/rewards.jpg') }}" alt="" title=""/></div>
                <div class="pad-pop">
                    <div class="pad-bdy">
                        <div class="t-pop">Free Car Wash and Vacuum</div>
                        <div class="text-center mb25">
                            <div class="radius-reward green">Awarded</div>
                        </div>
                        <div class="row row-pop">
                            <div class="col-4">
                                <div class="t1">Points</div>
                                <div class="t2">1,000 points</div>
                            </div>
                            <div class="col-4 text-center">
                                <div class="t1">Expiry Date</div>
                                <div class="t2">15 Feb 2022</div>
                            </div>
                            <div class="col-4 text-end">
                                <div class="t1">Date Awarded</div>
                                <div class="t2">15 Jan 2022</div>
                            </div>
                        </div>
                        <div class="bdr-pop"></div>
                        <div class="t-pop2 mb40">
                            <div class="mb25">
                                <p class="black">Rewards ID asd-werrtty-112234-fajsk</p>
                                <p>Montes, aenean ut magna tempus amet, non eu tincidunt morbi. Lacus, rhoncus, feugiat quisque posuere velit quis. Sed volutpat et leo feugiat aenean eleifend praesent. </p>
                            </div>
                            <div class="mb25">
                                <p>Voucher is auto-credited to your account. To redeem the voucher, follow the steps:</p>
                                <p>1 ) Book a Service with us</p>
                                <p>2 ) Flash the QR code you received from us via email</p>
                                <p>3 ) Your redemption is done!</p>
                            </div>
                            <div>
                                <p class="black">Terms and Conditions</p>
                                <ul>
                                    <li>Montes, aenean ut magna tempus amet, non eu tincidunt morbi.</li>
                                    <li>Lacus, rhoncus, feugiat quisque posuere velit quis.</li>
                                    <li>Sed volutpat et leo feugiat aenean eleifend praesent.</li>
                                    <li>Montes, aenean ut magna tempus amet, non eu tincidunt morbi.</li>
                                    <li>Lacus, rhoncus, feugiat quisque posuere velit quis.</li>
                                    <li>Sed volutpat et leo feugiat aenean eleifend praesent.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="btn-pop">
                            <a data-bs-dismiss="modal">
                                <button class="hvr-button" type="button">Close</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-rewards-expired" class="modal fade modal-global modal-rewards" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="img-pop"><img src="{{ asset('images/rewards.jpg') }}" alt="" title=""/></div>
                <div class="pad-pop">
                    <div class="pad-bdy">
                        <div class="t-pop">Free Car Wash and Vacuum</div>
                        <div class="text-center mb25">
                            <div class="radius-reward grey">Expired</div>
                            <div class="date-reward">Used on 15 Feb 2022</div>
                        </div>
                        <div class="row row-pop">
                            <div class="col-4">
                                <div class="t1">Points</div>
                                <div class="t2">1,000 points</div>
                            </div>
                            <div class="col-4 text-center">
                                <div class="t1">Expiry Date</div>
                                <div class="t2">15 Feb 2022</div>
                            </div>
                            <div class="col-4 text-end">
                                <div class="t1">Date Awarded</div>
                                <div class="t2">15 Jan 2022</div>
                            </div>
                        </div>
                        <div class="bdr-pop"></div>
                        <div class="t-pop2 mb40">
                            <div class="mb25">
                                <p class="black">Rewards ID asd-werrtty-112234-fajsk</p>
                                <p>Montes, aenean ut magna tempus amet, non eu tincidunt morbi. Lacus, rhoncus, feugiat quisque posuere velit quis. Sed volutpat et leo feugiat aenean eleifend praesent. </p>
                            </div>
                            <div class="mb25">
                                <p>Voucher is auto-credited to your account. To redeem the voucher, follow the steps:</p>
                                <p>1 ) Book a Service with us</p>
                                <p>2 ) Flash the QR code you received from us via email</p>
                                <p>3 ) Your redemption is done!</p>
                            </div>
                            <div>
                                <p class="black">Terms and Conditions</p>
                                <ul>
                                    <li>Montes, aenean ut magna tempus amet, non eu tincidunt morbi.</li>
                                    <li>Lacus, rhoncus, feugiat quisque posuere velit quis.</li>
                                    <li>Sed volutpat et leo feugiat aenean eleifend praesent.</li>
                                    <li>Montes, aenean ut magna tempus amet, non eu tincidunt morbi.</li>
                                    <li>Lacus, rhoncus, feugiat quisque posuere velit quis.</li>
                                    <li>Sed volutpat et leo feugiat aenean eleifend praesent.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="btn-pop">
                            <a data-bs-dismiss="modal">
                                <button class="hvr-button" type="button">Close</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-rewards-used" class="modal fade modal-global modal-rewards" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="img-pop"><img src="{{ asset('images/rewards.jpg') }}" alt="" title=""/></div>
                <div class="pad-pop">
                    <div class="pad-bdy">
                        <div class="t-pop">Free Car Wash and Vacuum</div>
                        <div class="text-center mb25">
                            <div class="radius-reward grey">Used</div>
                            <div class="date-reward">Used on 15 Feb 2022</div>
                        </div>
                        <div class="row row-pop">
                            <div class="col-4">
                                <div class="t1">Points</div>
                                <div class="t2">1,000 points</div>
                            </div>
                            <div class="col-4 text-center">
                                <div class="t1">Expiry Date</div>
                                <div class="t2">15 Feb 2022</div>
                            </div>
                            <div class="col-4 text-end">
                                <div class="t1">Date Awarded</div>
                                <div class="t2">15 Jan 2022</div>
                            </div>
                        </div>
                        <div class="bdr-pop"></div>
                        <div class="t-pop2 mb40">
                            <div class="mb25">
                                <p class="black">Rewards ID asd-werrtty-112234-fajsk</p>
                                <p>Montes, aenean ut magna tempus amet, non eu tincidunt morbi. Lacus, rhoncus, feugiat quisque posuere velit quis. Sed volutpat et leo feugiat aenean eleifend praesent. </p>
                            </div>
                            <div class="mb25">
                                <p>Voucher is auto-credited to your account. To redeem the voucher, follow the steps:</p>
                                <p>1 ) Book a Service with us</p>
                                <p>2 ) Flash the QR code you received from us via email</p>
                                <p>3 ) Your redemption is done!</p>
                            </div>
                            <div>
                                <p class="black">Terms and Conditions</p>
                                <ul>
                                    <li>Montes, aenean ut magna tempus amet, non eu tincidunt morbi.</li>
                                    <li>Lacus, rhoncus, feugiat quisque posuere velit quis.</li>
                                    <li>Sed volutpat et leo feugiat aenean eleifend praesent.</li>
                                    <li>Montes, aenean ut magna tempus amet, non eu tincidunt morbi.</li>
                                    <li>Lacus, rhoncus, feugiat quisque posuere velit quis.</li>
                                    <li>Sed volutpat et leo feugiat aenean eleifend praesent.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="btn-pop">
                            <a data-bs-dismiss="modal">
                                <button class="hvr-button" type="button">Close</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script type="text/javascript">
	$(document).ready(function() {
		$('.nav-dash').addClass('active');

        $('ul.l-account-resp li a').click(function(event) {
            $('ul.l-account').toggleClass('open');
        });
        
        $('header').addClass('account');
    });
</script>
@endsection