@extends('layout')

@section('content')
    <div class="banner-global" style="background: url('../images/banner-account.jpg') no-repeat center;">
        <div class="css-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="{{ URL::to('/') }}">Home</a></li>
                    <li>/</li>
                    <li><a href="{{ URL::to('/regular/dashboard') }}">My Account</a></li>
                </ul>
            </div>
        </div>
        <div class="tbl">
            <div class="cell">
                <div class="container">
                    <div class="title">My Account</div>
                </div>
            </div>
        </div>
    </div>
    <div class="css-account">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="left-account">
                        @include('member.regular.menu')
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="right-account">
                        <div class="title-account mb20">My Rewards</div>
                        <div class="box-profile-reward">
                            <div class="row">
                                <div class="col-md-6 my-auto">
                                    <div class="t1">10 000</div>
                                    <div class="t2">Points available</div>
                                </div>
                                <div class="col-md-6 my-auto text-end">
                                    <div class="btn-reward">
                                        <a href="{{ URL::to('/regular/rewards-catalogue') }}">Redeem rewards <i class="fa-solid fa-arrow-right-long"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="title-account mb20">Your available rewards</div>
                            <div class="row">
                                <div class="col-6 col-md-4 col-lg-3">
                                    <div class="item-reward">
                                        <a data-bs-toggle="modal" data-bs-target="#modal-rewards-awarded">
                                            <div class="img"><img src="{{ asset('images/reward1.jpg') }}" alt="" title=""/></div>
                                            <div class="status-reward green">AWARDED</div>
                                            <div class="t1">FREE Basic Servicing (Castrol)</div>
                                            <div class="t2">Valid until 15 Feb 2022</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-6 col-md-4 col-lg-3">
                                    <div class="item-reward">
                                        <a data-bs-toggle="modal" data-bs-target="#modal-rewards-awarded">
                                            <div class="img"><img src="{{ asset('images/reward1.jpg') }}" alt="" title=""/></div>
                                            <div class="status-reward green">AWARDED</div>
                                            <div class="t1">FREE Basic Servicing (Castrol)</div>
                                            <div class="t2">Valid until 15 Feb 2022</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-6 col-md-4 col-lg-3">
                                    <div class="item-reward">
                                        <a data-bs-toggle="modal" data-bs-target="#modal-rewards-awarded">
                                            <div class="img"><img src="{{ asset('images/reward1.jpg') }}" alt="" title=""/></div>
                                            <div class="status-reward green">AWARDED</div>
                                            <div class="t1">FREE Basic Servicing (Castrol)</div>
                                            <div class="t2">Valid until 15 Feb 2022</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-6 col-md-4 col-lg-3">
                                    <div class="item-reward">
                                        <a data-bs-toggle="modal" data-bs-target="#modal-rewards-awarded">
                                            <div class="img"><img src="{{ asset('images/reward1.jpg') }}" alt="" title=""/></div>
                                            <div class="status-reward green">AWARDED</div>
                                            <div class="t1">FREE Basic Servicing (Castrol)</div>
                                            <div class="t2">Valid until 15 Feb 2022</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="title-account mb20">Past rewards</div>
                            <div class="row">
                                <div class="col-6 col-md-4 col-lg-3">
                                    <div class="item-reward">
                                        <a data-bs-toggle="modal" data-bs-target="#modal-rewards-expired">
                                            <div class="img"><img src="{{ asset('images/reward2.jpg') }}" alt="" title=""/></div>
                                            <div class="status-reward grey">Expired</div>
                                            <div class="t1 grey">FREE Basic Servicing (Castrol)</div>
                                            <div class="t2">Valid until 15 Feb 2022</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-6 col-md-4 col-lg-3">
                                    <div class="item-reward">
                                        <a data-bs-toggle="modal" data-bs-target="#modal-rewards-expired">
                                            <div class="img"><img src="{{ asset('images/reward2.jpg') }}" alt="" title=""/></div>
                                            <div class="status-reward grey">Expired</div>
                                            <div class="t1 grey">FREE Basic Servicing (Castrol)</div>
                                            <div class="t2">Valid until 15 Feb 2022</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-6 col-md-4 col-lg-3">
                                    <div class="item-reward">
                                        <a data-bs-toggle="modal" data-bs-target="#modal-rewards-used">
                                            <div class="img"><img src="{{ asset('images/reward2.jpg') }}" alt="" title=""/></div>
                                            <div class="status-reward grey">Used</div>
                                            <div class="t1 grey">FREE Basic Servicing (Castrol)</div>
                                            <div class="t2">Valid until 15 Feb 2022</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-6 col-md-4 col-lg-3">
                                    <div class="item-reward">
                                        <a data-bs-toggle="modal" data-bs-target="#modal-rewards-used">
                                            <div class="img"><img src="{{ asset('images/reward2.jpg') }}" alt="" title=""/></div>
                                            <div class="status-reward grey">Used</div>
                                            <div class="t1 grey">FREE Basic Servicing (Castrol)</div>
                                            <div class="t2">Valid until 15 Feb 2022</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-rewards-awarded" class="modal fade modal-global modal-rewards" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="img-pop"><img src="{{ asset('images/rewards.jpg') }}" alt="" title=""/></div>
                <div class="pad-pop">
                    <div class="pad-bdy">
                        <div class="t-pop">Free Car Wash and Vacuum</div>
                        <div class="text-center mb25">
                            <div class="radius-reward green">Awarded</div>
                        </div>
                        <div class="row row-pop">
                            <div class="col-4">
                                <div class="t1">Points</div>
                                <div class="t2">1,000 points</div>
                            </div>
                            <div class="col-4 text-center">
                                <div class="t1">Expiry Date</div>
                                <div class="t2">15 Feb 2022</div>
                            </div>
                            <div class="col-4 text-end">
                                <div class="t1">Date Awarded</div>
                                <div class="t2">15 Jan 2022</div>
                            </div>
                        </div>
                        <div class="bdr-pop"></div>
                        <div class="t-pop2 mb40">
                            <div class="mb25">
                                <p class="black">Rewards ID asd-werrtty-112234-fajsk</p>
                                <p>Montes, aenean ut magna tempus amet, non eu tincidunt morbi. Lacus, rhoncus, feugiat quisque posuere velit quis. Sed volutpat et leo feugiat aenean eleifend praesent. </p>
                            </div>
                            <div class="mb25">
                                <p>Voucher is auto-credited to your account. To redeem the voucher, follow the steps:</p>
                                <p>1 ) Book a Service with us</p>
                                <p>2 ) Flash the QR code you received from us via email</p>
                                <p>3 ) Your redemption is done!</p>
                            </div>
                            <div>
                                <p class="black">Terms and Conditions</p>
                                <ul>
                                    <li>Montes, aenean ut magna tempus amet, non eu tincidunt morbi.</li>
                                    <li>Lacus, rhoncus, feugiat quisque posuere velit quis.</li>
                                    <li>Sed volutpat et leo feugiat aenean eleifend praesent.</li>
                                    <li>Montes, aenean ut magna tempus amet, non eu tincidunt morbi.</li>
                                    <li>Lacus, rhoncus, feugiat quisque posuere velit quis.</li>
                                    <li>Sed volutpat et leo feugiat aenean eleifend praesent.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="btn-pop">
                            <a data-bs-dismiss="modal">
                                <button class="hvr-button" type="button">Close</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-rewards-expired" class="modal fade modal-global modal-rewards" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="img-pop"><img src="{{ asset('images/rewards.jpg') }}" alt="" title=""/></div>
                <div class="pad-pop">
                    <div class="pad-bdy">
                        <div class="t-pop">Free Car Wash and Vacuum</div>
                        <div class="text-center mb25">
                            <div class="radius-reward grey">Expired</div>
                            <div class="date-reward">Used on 15 Feb 2022</div>
                        </div>
                        <div class="row row-pop">
                            <div class="col-4">
                                <div class="t1">Points</div>
                                <div class="t2">1,000 points</div>
                            </div>
                            <div class="col-4 text-center">
                                <div class="t1">Expiry Date</div>
                                <div class="t2">15 Feb 2022</div>
                            </div>
                            <div class="col-4 text-end">
                                <div class="t1">Date Awarded</div>
                                <div class="t2">15 Jan 2022</div>
                            </div>
                        </div>
                        <div class="bdr-pop"></div>
                        <div class="t-pop2 mb40">
                            <div class="mb25">
                                <p class="black">Rewards ID asd-werrtty-112234-fajsk</p>
                                <p>Montes, aenean ut magna tempus amet, non eu tincidunt morbi. Lacus, rhoncus, feugiat quisque posuere velit quis. Sed volutpat et leo feugiat aenean eleifend praesent. </p>
                            </div>
                            <div class="mb25">
                                <p>Voucher is auto-credited to your account. To redeem the voucher, follow the steps:</p>
                                <p>1 ) Book a Service with us</p>
                                <p>2 ) Flash the QR code you received from us via email</p>
                                <p>3 ) Your redemption is done!</p>
                            </div>
                            <div>
                                <p class="black">Terms and Conditions</p>
                                <ul>
                                    <li>Montes, aenean ut magna tempus amet, non eu tincidunt morbi.</li>
                                    <li>Lacus, rhoncus, feugiat quisque posuere velit quis.</li>
                                    <li>Sed volutpat et leo feugiat aenean eleifend praesent.</li>
                                    <li>Montes, aenean ut magna tempus amet, non eu tincidunt morbi.</li>
                                    <li>Lacus, rhoncus, feugiat quisque posuere velit quis.</li>
                                    <li>Sed volutpat et leo feugiat aenean eleifend praesent.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="btn-pop">
                            <a data-bs-dismiss="modal">
                                <button class="hvr-button" type="button">Close</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-rewards-used" class="modal fade modal-global modal-rewards" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="img-pop"><img src="{{ asset('images/rewards.jpg') }}" alt="" title=""/></div>
                <div class="pad-pop">
                    <div class="pad-bdy">
                        <div class="t-pop">Free Car Wash and Vacuum</div>
                        <div class="text-center mb25">
                            <div class="radius-reward grey">Used</div>
                            <div class="date-reward">Used on 15 Feb 2022</div>
                        </div>
                        <div class="row row-pop">
                            <div class="col-4">
                                <div class="t1">Points</div>
                                <div class="t2">1,000 points</div>
                            </div>
                            <div class="col-4 text-center">
                                <div class="t1">Expiry Date</div>
                                <div class="t2">15 Feb 2022</div>
                            </div>
                            <div class="col-4 text-end">
                                <div class="t1">Date Awarded</div>
                                <div class="t2">15 Jan 2022</div>
                            </div>
                        </div>
                        <div class="bdr-pop"></div>
                        <div class="t-pop2 mb40">
                            <div class="mb25">
                                <p class="black">Rewards ID asd-werrtty-112234-fajsk</p>
                                <p>Montes, aenean ut magna tempus amet, non eu tincidunt morbi. Lacus, rhoncus, feugiat quisque posuere velit quis. Sed volutpat et leo feugiat aenean eleifend praesent. </p>
                            </div>
                            <div class="mb25">
                                <p>Voucher is auto-credited to your account. To redeem the voucher, follow the steps:</p>
                                <p>1 ) Book a Service with us</p>
                                <p>2 ) Flash the QR code you received from us via email</p>
                                <p>3 ) Your redemption is done!</p>
                            </div>
                            <div>
                                <p class="black">Terms and Conditions</p>
                                <ul>
                                    <li>Montes, aenean ut magna tempus amet, non eu tincidunt morbi.</li>
                                    <li>Lacus, rhoncus, feugiat quisque posuere velit quis.</li>
                                    <li>Sed volutpat et leo feugiat aenean eleifend praesent.</li>
                                    <li>Montes, aenean ut magna tempus amet, non eu tincidunt morbi.</li>
                                    <li>Lacus, rhoncus, feugiat quisque posuere velit quis.</li>
                                    <li>Sed volutpat et leo feugiat aenean eleifend praesent.</li>
                                </ul>
                            </div>
                        </div>
                        <div class="btn-pop">
                            <a data-bs-dismiss="modal">
                                <button class="hvr-button" type="button">Close</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

<script type="text/javascript">
	$(document).ready(function() {
		$('.nav-reward').addClass('active');

        $('ul.l-account-resp li a').click(function(event) {
            $('ul.l-account').toggleClass('open');
        });

        $('.css-no-account').addClass('active');
        
        $('.css-account').addClass('active');

        $('header').addClass('account');
	});
</script>
@endsection