<div class="box-menu-account">
	<ul class="l-account-resp">
		<li>
			<a class="nav-dash">
				<img class="img img1" src="{{ asset('images/dashboard.png') }}" alt="" title=""/>
				<img class="img img2" src="{{ asset('images/dashboard2.png') }}" alt="" title=""/>
				My Dashboard <i class="fa-solid fa-chevron-down"></i>
			</a>
		</li>
		<li>
			<a class="nav-info">
				<img class="img img1" src="{{ asset('images/information.png') }}" alt="" title=""/>
				<img class="img img2" src="{{ asset('images/information2.png') }}" alt="" title=""/>
				Account Information <i class="fa-solid fa-chevron-down"></i>
			</a>
		</li>
		<li>
			<a class="nav-book">
				<img class="img img1" src="{{ asset('images/bookings.png') }}" alt="" title=""/>
				<img class="img img2" src="{{ asset('images/bookings2.png') }}" alt="" title=""/>
				My Bookings <i class="fa-solid fa-chevron-down"></i>
			</a>
		</li>
	</ul>
</div>
<ul class="l-account">
	<li>
		<a href="{{ URL::to('/business/dashboard') }}" class="nav-dash">
			<img class="img img1" src="{{ asset('images/dashboard.png') }}" alt="" title=""/>
			<img class="img img2" src="{{ asset('images/dashboard2.png') }}" alt="" title=""/>
			My Dashboard
		</a>
	</li>
	<li>
		<a href="{{ URL::to('/business/account-information') }}" class="nav-info">
			<img class="img img1" src="{{ asset('images/information.png') }}" alt="" title=""/>
			<img class="img img2" src="{{ asset('images/information2.png') }}" alt="" title=""/>
			Account Information
		</a>
	</li>
	<li>
		<a href="{{ URL::to('/business/my-bookings') }}" class="nav-book">
			<img class="img img1" src="{{ asset('images/bookings.png') }}" alt="" title=""/>
			<img class="img img2" src="{{ asset('images/bookings2.png') }}" alt="" title=""/>
			My Bookings
		</a>
	</li>
	<li>
		<a href="{{ URL::to('/') }}">
			<img class="img img1" src="{{ asset('images/catalogue.png') }}" alt="" title=""/>
			<img class="img img2" src="{{ asset('images/catalogue2.png') }}" alt="" title=""/>
			Logout
		</a>
	</li>
</ul>