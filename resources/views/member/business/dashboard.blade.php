@extends('layout')

@section('content')
    <div class="banner-global" style="background: url('../images/banner-account.jpg') no-repeat center;">
        <div class="css-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="{{ URL::to('/') }}">Home</a></li>
                    <li>/</li>
                    <li><a href="{{ URL::to('/business/dashboard') }}">My Account</a></li>
                </ul>
            </div>
        </div>
        <div class="tbl">
            <div class="cell">
                <div class="container">
                    <div class="title">My Account</div>
                </div>
            </div>
        </div>
    </div>
    <div class="css-account">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="left-account">
                        @include('member.business.menu')
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="right-account">
                        <div class="box-account">
                            <div class="name">Welcome back David!</div>
                            <div class="bdy">
                                <p>Montes, aenean ut magna tempus amet, non eu tincidunt morbi. Lacus, rhoncus, feugiat quisque posuere velit quis. Sed volutpat et leo feugiat aenean eleifend praesent.</p>
                            </div>
                            <div class="img-profile"><img src="{{ asset('images/profile.png') }}" alt="" title=""/></div>
                        </div>
                        <div class="mb70">
                            <div class="row">
                                <div class="col-lg-6 my-auto">
                                    <div class="title-account">Personal details</div>
                                    <div class="link"><a href="{{ URL::to('/business/account-information') }}">Edit</a></div>
                                    <form class="css-form">
                                        <div class="form-group">
                                            <label id="name">Name</label>
                                            <input class="form-control" id="name" name="name" value="David Lim" type="text" readonly="" />
                                        </div> 
                                        <div class="form-group">
                                            <label id="email">Email</label>
                                            <input class="form-control" id="email" name="email" type="text" value="davidlim@gmail.com" readonly="" />
                                        </div>  
                                        <div class="form-group mb0">
                                            <label id="phone">Contact Number</label>
                                            <input class="form-control only-number" id="phone" name="phone" type="text" value="9008 7665" readonly="" />
                                        </div>
                                    </form>
                                </div>
                                <div class="col-lg-6">
                                    <div class="title-account mb20">Upcoming appointments</div>
                                    <div class="box-appo">
                                        <div class="tbl">
                                            <div class="cell wdate">
                                                <div class="in">
                                                    <div class="date">25</div>
                                                    <div class="day">Mon</div>
                                                </div>
                                            </div>
                                            <div class="cell">
                                                <div class="t">Car rim replacement</div>
                                                <div class="time">10:00 am</div>
                                            </div>
                                        </div>
                                        <div class="view">
                                            <a href="{{ URL::to('/regular/my-bookings') }}">View</a>
                                        </div>
                                    </div>
                                    <div class="box-appo">
                                        <div class="tbl">
                                            <div class="cell wdate">
                                                <div class="in">
                                                    <div class="date">25</div>
                                                    <div class="day">Mon</div>
                                                </div>
                                            </div>
                                            <div class="cell">
                                                <div class="t">Car rim replacement</div>
                                                <div class="time">10:00 am</div>
                                            </div>
                                        </div>
                                        <div class="view">
                                            <a href="{{ URL::to('/regular/my-bookings') }}">View</a>
                                        </div>
                                    </div>
                                    <div class="box-appo">
                                        <div class="tbl">
                                            <div class="cell wdate">
                                                <div class="in">
                                                    <div class="date">25</div>
                                                    <div class="day">Mon</div>
                                                </div>
                                            </div>
                                            <div class="cell">
                                                <div class="t">Car rim replacement</div>
                                                <div class="time">10:00 am</div>
                                            </div>
                                        </div>
                                        <div class="view">
                                            <a href="{{ URL::to('/regular/my-bookings') }}">View</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script type="text/javascript">
	$(document).ready(function() {
		$('.nav-dash').addClass('active');

        $('ul.l-account-resp li a').click(function(event) {
            $('ul.l-account').toggleClass('open');
        });

        $('header').addClass('account');
    });
</script>
@endsection