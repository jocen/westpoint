@extends('layout')

@section('content')
    <div class="banner-global" style="background: url('../images/banner-account.jpg') no-repeat center;">
        <div class="css-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="{{ URL::to('/') }}">Home</a></li>
                    <li>/</li>
                    <li><a href="{{ URL::to('/business/dashboard') }}">My Account</a></li>
                </ul>
            </div>
        </div>
        <div class="tbl">
            <div class="cell">
                <div class="container">
                    <div class="title">My Account</div>
                </div>
            </div>
        </div>
    </div>
    <div class="css-account">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="left-account">
                        @include('member.business.menu')
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="right-account">
                        <div class="row">
                            <div class="col-md-11 col-lg-9">
                                <form class="css-form">
                                    <div class="mb70">
                                        <div class="title-account mb20">Company information</div>
                                        <div class="form-group">
                                            <label id="company-name">Company Name</label>
                                            <input class="form-control" id="company-name" name="company-name" value="David Lim" type="text" required="" />
                                        </div>
                                        <div class="form-group">
                                            <label id="company-uen">Company UEN</label>
                                            <input class="form-control" id="company-uen" name="company-uen" value="2019029374T" type="text" required="" />
                                        </div>
                                    </div>
                                    <div class="mb70">
                                        <div class="title-account mb20">Contact information</div>
                                        <div class="form-group">
                                            <label id="phone">Contact Number</label>
                                            <input class="form-control only-number" id="phone" name="phone" type="text" value="9008 7665" required="" />
                                        </div>
                                        <div class="form-group">
                                            <label id="email">Email</label>
                                            <input class="form-control" id="email" name="email" type="text" value="davidlim@gmail.com" required="" />
                                        </div>
                                    </div>
                                    <div>
                                        <div class="title-account2">Address</div>
                                        <div class="form-group">
                                            <label>Unit - Block No</label>
                                            <ul class="l-address">
                                                <li>
                                                    <input class="form-control" id="unit" name="unit" value="12" type="text" required="" />
                                                </li>
                                                <li>-</li>
                                                <li>
                                                    <input class="form-control" id="block-no" name="block-no" value="230" type="text" required="" />
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="form-group">
                                            <label id="street-name">Street Name</label>
                                            <input class="form-control" id="street-name" name="street-name" value="23 Jalan Dukun" type="text" required="" />
                                        </div>
                                        <div class="form-group">
                                            <label id="building-name">Building Name</label>
                                            <input class="form-control" id="building-name" name="building-name" type="text" value="Building" required="" />
                                        </div>
                                        <div class="form-group">
                                            <label id="postal-code">Postal Code</label>
                                            <input class="form-control" id="postal-code" name="postal-code" type="text" value="S 560987" required="" />
                                        </div>
                                        <div class="form-group">
                                            <label id="password">Password</label>
                                            <input class="form-control only-number" id="password" name="password" type="password" value="" required="" />
                                        </div>
                                    </div>
                                    <div class="button-account">
                                        <button class="hvr-button" type="submit">Save Changes</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

<script type="text/javascript">
	$(document).ready(function() {
		$('.nav-info').addClass('active');

        $('header').addClass('account');

        $('ul.l-account-resp li a').click(function(event) {
            $('ul.l-account').toggleClass('open');
        });
	});
</script>
@endsection