@extends('layout')

@section('content')
    <div class="banner-global" style="background: url('../images/banner-account.jpg') no-repeat center;">
        <div class="css-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="{{ URL::to('/') }}">Home</a></li>
                    <li>/</li>
                    <li><a href="{{ URL::to('/business/dashboard') }}">My Account</a></li>
                </ul>
            </div>
        </div>
        <div class="tbl">
            <div class="cell">
                <div class="container">
                    <div class="title">My Account</div>
                </div>
            </div>
        </div>
    </div>
    <div class="css-account">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="left-account">
                        @include('member.business.menu')
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="right-account">
                        <div class="title-account">My bookings</div>
                        <ul class="nav l-booking" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="upcoming-tab" data-bs-toggle="pill" href="#upcoming" role="tab" aria-controls="upcoming" aria-selected="true">
                                    <div class="num"><span class="num-upcoming"></span></div>
                                    <div>Upcoming</div>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="history-tab" data-bs-toggle="pill" href="#history" role="tab" aria-controls="history" aria-selected="false">
                                    <div class="num"><span class="num-history"></span></div>
                                    <div>History</div>
                                </a>
                            </li>
                        </ul>
                         <div class="tab-content">
                            <div class="tab-pane fade show active" id="upcoming" role="tabpanel" aria-labelledby="upcoming-tab">
                                <div class="box-booking">
                                    <div class="t-show">Detail</div>
                                    <div class="item-booking">
                                        <div class="tbl-booking tbl-hdr">
                                            <div class="tbl">
                                                <div class="cell w1">Appointment</div>
                                                <div class="cell w2">Date</div>
                                                <div class="cell w3">Time</div>
                                                <div class="cell w4">Status</div>
                                                <div class="cell w5">&nbsp;</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-booking item-upcoming" id="data-upcoming-1">
                                        <div class="tbl-booking tbl-bdy">
                                            <div class="tbl">
                                                <div class="cell w1">
                                                    <div class="t1">Car Rim Replacement</div>
                                                    <div class="t2">Bookingrefid-367-23456Y</div>
                                                </div>
                                                <div class="cell w2"><span class="show">Date:</span> 15 February 2022</div>
                                                <div class="cell w3"><span class="show">Time:</span> 03:00PM</div>
                                                <div class="cell w4"><span class="show">Status:</span> <span class="green">Upcoming</span></div>
                                                <div class="cell w5">
                                                    <ul class="l-edit">
                                                        <li><a class="click-edit"><i class="fas fa-edit"></i></a></li>
                                                        <li><a class="click-get-data-upcoming" data-bs-toggle="modal" data-bs-target="#modal-cancel" data-upcoming="1"><i class="fas fa-times-circle"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-booking">
                                            <form>
                                                <div class="row">
                                                    <div class="col-lg-8">
                                                        <div class="form-group form-date">
                                                            <label>*Select Date</label>
                                                            <div class="pos-rel">
                                                                <input class="form-control date" type="text" required="" readonly="" />
                                                                <div class="abs"><img src="{{ asset('images/date.png') }}" alt="" title=""/></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label id="time-slot">*Select Time Slot</label>
                                                    <ul class="l-time">
                                                        <li><a>11:00AM</a></li>
                                                        <li><a>12:00PM</a></li>
                                                        <li><a>04:00PM</a></li>
                                                    </ul>
                                                </div>
                                                <div class="btn-booking">
                                                    <button class="hvr-button" type="submit">Save</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="item-booking item-upcoming" id="data-upcoming-2">
                                        <div class="tbl-booking tbl-bdy">
                                            <div class="tbl">
                                                <div class="cell w1">
                                                    <div class="t1">Car Rim Replacement</div>
                                                    <div class="t2">Bookingrefid-367-23456Y</div>
                                                </div>
                                                <div class="cell w2"><span class="show">Date:</span> 15 February 2022</div>
                                                <div class="cell w3"><span class="show">Time:</span> 03:00PM</div>
                                                <div class="cell w4"><span class="show">Status:</span> <span class="green">Upcoming</span></div>
                                                <div class="cell w5">
                                                    <ul class="l-edit">
                                                        <li><a class="click-edit"><i class="fas fa-edit"></i></a></li>
                                                        <li><a class="click-get-data-upcoming" data-bs-toggle="modal" data-bs-target="#modal-cancel" data-upcoming="2"><i class="fas fa-times-circle"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-booking">
                                            <form>
                                                <div class="row">
                                                    <div class="col-lg-8">
                                                        <div class="form-group form-date">
                                                            <label>*Select Date</label>
                                                            <div class="pos-rel">
                                                                <input class="form-control date" type="text" required="" readonly="" />
                                                                <div class="abs"><img src="{{ asset('images/date.png') }}" alt="" title=""/></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label id="time-slot">*Select Time Slot</label>
                                                    <ul class="l-time">
                                                        <li><a>11:00AM</a></li>
                                                        <li><a>12:00PM</a></li>
                                                        <li><a>04:00PM</a></li>
                                                    </ul>
                                                </div>
                                                <div class="btn-booking">
                                                    <button class="hvr-button" type="submit">Save</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="item-booking item-upcoming" id="data-upcoming-3">
                                        <div class="tbl-booking tbl-bdy">
                                            <div class="tbl">
                                                <div class="cell w1">
                                                    <div class="t1">Car Rim Replacement</div>
                                                    <div class="t2">Bookingrefid-367-23456Y</div>
                                                </div>
                                                <div class="cell w2"><span class="show">Date:</span> 15 February 2022</div>
                                                <div class="cell w3"><span class="show">Time:</span> 03:00PM</div>
                                                <div class="cell w4"><span class="show">Status:</span> <span class="green">Upcoming</span></div>
                                                <div class="cell w5">
                                                    <ul class="l-edit">
                                                        <li><a class="click-edit"><i class="fas fa-edit"></i></a></li>
                                                        <li><a class="click-get-data-upcoming" data-bs-toggle="modal" data-bs-target="#modal-cancel" data-upcoming="3"><i class="fas fa-times-circle"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-booking">
                                            <form>
                                                <div class="row">
                                                    <div class="col-lg-8">
                                                        <div class="form-group form-date">
                                                            <label>*Select Date</label>
                                                            <div class="pos-rel">
                                                                <input class="form-control date" type="text" required="" readonly="" />
                                                                <div class="abs"><img src="{{ asset('images/date.png') }}" alt="" title=""/></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label id="time-slot">*Select Time Slot</label>
                                                    <ul class="l-time">
                                                        <li><a>11:00AM</a></li>
                                                        <li><a>12:00PM</a></li>
                                                        <li><a>04:00PM</a></li>
                                                    </ul>
                                                </div>
                                                <div class="btn-booking">
                                                    <button class="hvr-button" type="submit">Save</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="item-booking item-upcoming" id="data-upcoming-4">
                                        <div class="tbl-booking tbl-bdy">
                                            <div class="tbl">
                                                <div class="cell w1">
                                                    <div class="t1">Car Rim Replacement</div>
                                                    <div class="t2">Bookingrefid-367-23456Y</div>
                                                </div>
                                                <div class="cell w2"><span class="show">Date:</span> 15 February 2022</div>
                                                <div class="cell w3"><span class="show">Time:</span> 03:00PM</div>
                                                <div class="cell w4"><span class="show">Status:</span> <span class="green">Upcoming</span></div>
                                                <div class="cell w5">
                                                    <ul class="l-edit">
                                                        <li><a class="click-edit"><i class="fas fa-edit"></i></a></li>
                                                        <li><a class="click-get-data-upcoming" data-bs-toggle="modal" data-bs-target="#modal-cancel" data-upcoming="4"><i class="fas fa-times-circle"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-booking">
                                            <form>
                                                <div class="row">
                                                    <div class="col-lg-8">
                                                        <div class="form-group form-date">
                                                            <label>*Select Date</label>
                                                            <div class="pos-rel">
                                                                <input class="form-control date" type="text" required="" readonly="" />
                                                                <div class="abs"><img src="{{ asset('images/date.png') }}" alt="" title=""/></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label id="time-slot">*Select Time Slot</label>
                                                    <ul class="l-time">
                                                        <li><a>11:00AM</a></li>
                                                        <li><a>12:00PM</a></li>
                                                        <li><a>04:00PM</a></li>
                                                    </ul>
                                                </div>
                                                <div class="btn-booking">
                                                    <button class="hvr-button" type="submit">Save</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="history" role="tabpanel" aria-labelledby="history-tab">
                                <div class="box-booking">
                                    <div class="t-show">Detail</div>
                                    <div class="item-booking">
                                        <div class="tbl-booking tbl-hdr">
                                            <div class="tbl">
                                                <div class="cell w1">Appointment</div>
                                                <div class="cell w2">Date</div>
                                                <div class="cell w3">Time</div>
                                                <div class="cell w4">Status</div>
                                                <div class="cell w5">&nbsp;</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-booking item-history">
                                        <div class="tbl-booking tbl-bdy">
                                            <div class="tbl">
                                                <div class="cell w1">
                                                    <div class="t1">Car Rim Replacement</div>
                                                    <div class="t2">Bookingrefid-367-23456Y</div>
                                                </div>
                                                <div class="cell w2"><span class="show">Date:</span> 15 February 2022</div>
                                                <div class="cell w3"><span class="show">Time:</span> 03:00PM</div>
                                                <div class="cell w4"><span class="show">Status:</span> <span class="red">Completed</span></div>
                                                <div class="cell w5">
                                                    <ul class="l-edit">
                                                        <li><a class="click-pdf" href="" download><img src="{{ asset('images/pdf.png') }}" alt="" title=""/></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-booking item-history">
                                        <div class="tbl-booking tbl-bdy">
                                            <div class="tbl">
                                                <div class="cell w1">
                                                    <div class="t1">Car Rim Replacement</div>
                                                    <div class="t2">Bookingrefid-367-23456Y</div>
                                                </div>
                                                <div class="cell w2"><span class="show">Date:</span> 15 February 2022</div>
                                                <div class="cell w3"><span class="show">Time:</span> 03:00PM</div>
                                                <div class="cell w4"><span class="show">Status:</span> <span class="red">Completed</span></div>
                                                <div class="cell w5">
                                                    <ul class="l-edit">
                                                        <li><a class="click-pdf" href="" download><img src="{{ asset('images/pdf.png') }}" alt="" title=""/></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-booking item-history">
                                        <div class="tbl-booking tbl-bdy">
                                            <div class="tbl">
                                                <div class="cell w1">
                                                    <div class="t1">Car Rim Replacement</div>
                                                    <div class="t2">Bookingrefid-367-23456Y</div>
                                                </div>
                                                <div class="cell w2"><span class="show">Date:</span> 15 February 2022</div>
                                                <div class="cell w3"><span class="show">Time:</span> 03:00PM</div>
                                                <div class="cell w4"><span class="show">Status:</span> <span class="red">Completed</span></div>
                                                <div class="cell w5">
                                                    <ul class="l-edit">
                                                        <li><a class="click-pdf" href="" download><img src="{{ asset('images/pdf.png') }}" alt="" title=""/></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-cancel" class="modal fade modal-global" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="pad-pop">
                    <div class="pad-bdy">
                        <div class="img-success">
                            <img src="{{ asset('images/cancel.png') }}" alt="" title=""/>
                        </div>
                        <div class="t-pop">Cancel Booking?</div>
                        <div class="t-pop2">
                            <p>Are you sure you want to cancel your booking? Your time slot may not be available anymore should you decide to rebook.</p>
                        </div>
                        <ul class="l-btn">
                            <li>
                                <a class="click-yes-cancel">
                                    <button class="hvr-button" type="button">Yes, cancel</button>
                                </a>
                            </li>
                            <li>
                                <a data-bs-dismiss="modal">
                                    <button class="hvr-button grey" type="button">No, go back</button>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
<script type="text/javascript">
	$(document).ready(function() {
		$('.nav-book').addClass('active');

        $('header').addClass('account');

        $('ul.l-time li a').click(function(event) {
            $('ul.l-time li a').removeClass('active');
            $(this).addClass('active');
        });

        $(".date").datepicker({
            changeMonth: true,
            minDate: 0,
            changeYear: true,
            dateFormat: 'dd MM yy',
            beforeShowDay: $.datepicker.noWeekends,
        });

        $('.click-edit').click(function(event) {
            $(this).parents('.item-booking').find('.tbl-bdy').toggleClass('open');
            $(this).parents('.item-booking').find('.form-booking').toggleClass('open');
        });

        $('ul.l-account-resp li a').click(function(event) {
            $('ul.l-account').toggleClass('open');
        });

        var getCountUpcoming = $(".item-upcoming").length;
        var formatUpcoming = ("0" + getCountUpcoming).slice(-2);

        var getCountHistory = $(".item-history").length;
        var formatHistory = ("0" + getCountHistory).slice(-2);

        $('.num-upcoming').html(formatUpcoming);
        $('.num-history').html(formatHistory);

        $('.click-get-data-upcoming').click(function(event) {
            var getDataUpcoming = $(this).attr('data-upcoming');
            $('.click-yes-cancel').click(function(event) {
                $('#modal-cancel').modal('hide');
                $('#data-upcoming-'+ getDataUpcoming +'').remove();
            });
        });
	});
</script>
@endsection