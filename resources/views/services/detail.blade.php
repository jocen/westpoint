@extends('layout')

@section('content')
    <div class="banner-global" style="background: url('images/banner-service-detail.jpg') no-repeat center;">
        <div class="css-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="{{ URL::to('/') }}">Home</a></li>
                    <li>/</li>
                    <li><a href="{{ URL::to('/services') }}">Services</a></li>
                    <li>/</li>
                    <li><a href="{{ URL::to('/services-detail') }}">Service & Maintenance</a></li>
                </ul>
            </div>
        </div>
        <div class="tbl">
            <div class="cell">
                <div class="container">
                    <div class="title">Service & Maintenance</div>
                    <div class="link">
                        <a href="{{ URL::to('/appointment') }}"><button class="hvr-button" type="button">BOOK APPOINTMENT</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="css-services mb0">
        <div class="container">
            <div class="t1">Service & Maintenance</div>
            <div class="t2">
                <p>Montes, aenean ut magna tempus amet, non eu tincidunt morbi. Lacus, rhoncus, feugiat quisque posuere velit quis. Sed volutpat et leo feugiat aenean eleifend praesent. Proin mauris vulputate adipiscing lacus consequat. Commodo neque malesuada rutrum ante. </p><br/>
                <p>Ornare commodo eros, feugiat vitae potenti condimentum. Felis pretium, varius vel erat ut. Enim pellentesque lacus, iaculis maecenas volutpat nunc elementum tempus vel. Hendrerit eget ornare in suspendisse varius sed. Et suspendisse lacinia morbi vestibulum elementum phasellus. Enim scelerisque ut nulla donec nulla.</p>
            </div>
            <div class="in-services">
                <div class="row">
                    <div class="col-md-6 my-auto">
                        <div class="img"><img src="{{ asset('images/bg-services.jpg') }}" alt="" title=""/></div>
                    </div>
                    <div class="col-md-6 my-auto">
                        <div class="title">Visit Westpoint’s workshop at:</div>
                        <div class="address">
                            <p>123 Chai Chee Drive,</p>
                            <p>Singapore 6709004</p>
                        </div>
                        <div class="link">
                            <a href="{{ URL::to('/appointment') }}"><button class="hvr-button" type="button">BOOK Now</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="banner-servies">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 my-auto">
                        <div class="title">How can we help?</div>
                        <div class="text">
                            <p>Our helpdesk is available from 9am - 6pm daily.</p>
                        </div>
                    </div>
                    <div class="col-md-5 my-auto">
                        <div class="link">
                            <a href="{{ URL::to('/') }}"><button class="hvr-button" type="button">Chat with us</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-services-product">
            <div class="container">
                <div class="t-product">You might be interested in these products </div>
                <div class="slider-services">
                    <div class="item">
                        <a href="{{ URL::to('/services-detail') }}">
                            <div class="img"><img src="{{ asset('images/item-services.jpg') }}" alt="" title=""/></div>
                            <div class="nm">Double Bronze 10 Spoke</div>
                            <div class="bdy">
                                <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                            </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="{{ URL::to('/services-detail') }}">
                            <div class="img"><img src="{{ asset('images/item-services.jpg') }}" alt="" title=""/></div>
                            <div class="nm">Double Bronze 10 Spoke</div>
                            <div class="bdy">
                                <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                            </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="{{ URL::to('/services-detail') }}">
                            <div class="img"><img src="{{ asset('images/item-services.jpg') }}" alt="" title=""/></div>
                            <div class="nm">Double Bronze 10 Spoke</div>
                            <div class="bdy">
                                <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                            </div>
                        </a>
                    </div>
                    <div class="item">
                        <a href="{{ URL::to('/services-detail') }}">
                            <div class="img"><img src="{{ asset('images/item-services.jpg') }}" alt="" title=""/></div>
                            <div class="nm">Double Bronze 10 Spoke</div>
                            <div class="bdy">
                                <p>Mattis pellentesque amet molestie tristique amet feugiat varius id leo turpis egestas scelerisque.</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
<script type="text/javascript">
	$(document).ready(function() {
		$('.nav-services').addClass('active');

        $('.slider-services').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: false,
            arrows: false,
            centerMode: false,
            focusOnSelect: true,
            infinite: false,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        arrows: true,
                        infinite: true
                    }
                }
            ]
        });
	});
</script>
@endsection