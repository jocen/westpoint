@extends('layout')

@section('content')
    <div class="banner-global" style="background: url('images/banner-service.jpg') no-repeat center;">
        <div class="css-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="{{ URL::to('/') }}">Home</a></li>
                    <li>/</li>
                    <li><a href="{{ URL::to('/services') }}">Services</a></li>
                </ul>
            </div>
        </div>
        <div class="tbl">
            <div class="cell">
                <div class="container">
                    <div class="title">Services</div>
                    <div class="bdy">
                        <p>Id risus est pellentesque tristique cras malesuada ante varius. Tempor, mi elementum consectetur ut ante dignissim pulvinar sit. Amet nibh massa quam maecenas in sit.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="css-services">
        <div class="container">
            <div class="t1">Browse Services by Service Category</div>
            <div class="controls">
                <div class="row">
                    <div class="col-6 col-md-3">
                        <button type="button" class="control" data-filter="all">All</button>
                    </div>
                    <div class="col-6 col-md-3">
                        <button type="button" class="control" data-filter=".servicing">
                            Servicing
                            <img class="img img1" src="{{ asset('images/servicing.png') }}" alt="" title=""/>
                            <img class="img img2" src="{{ asset('images/servicing2.png') }}" alt="" title=""/>
                        </button>
                    </div>
                    <div class="col-6 col-md-3">
                        <button type="button" class="control" data-filter=".detailing">
                            Detailing
                            <img class="img img1" src="{{ asset('images/detailing.png') }}" alt="" title=""/>
                            <img class="img img2" src="{{ asset('images/detailing2.png') }}" alt="" title=""/>
                        </button>
                    </div>
                    <div class="col-6 col-md-3">
                        <button type="button" class="control" data-filter=".dealership">
                            Car dealership
                            <img class="img img1" src="{{ asset('images/dealership.png') }}" alt="" title=""/>
                            <img class="img img2" src="{{ asset('images/dealership2.png') }}" alt="" title=""/>
                        </button>
                    </div>
                </div>
            </div>

            <div class="box-services">
                <div class="mix detailing">
                    <div class="tbl">
                        <div class="cell w-img">
                            <img src="{{ asset('images/mix2.jpg') }}" alt="" title=""/>
                        </div>
                        <div class="cell w-text">
                            <div class="t-mix"><a href="{{ URL::to('/services-detail') }}">Detailing</a></div>
                            <div class="bdy-mix">
                                <p>Id risus est pellentesque tristique cras malesuada ante varius. Tempor, mi elementum consectetur ut ante dignissim pulvinar sit. Amet nibh massa quam maecenas in sit. </p>
                            </div>
                        </div>
                        <div class="cell btn-mix">
                            <a href="{{ URL::to('/appointment') }}">
                                <button class="hvr-button" type="button">Book now</button>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="mix detailing">
                    <div class="tbl">
                        <div class="cell w-img">
                            <img src="{{ asset('images/mix2.jpg') }}" alt="" title=""/>
                        </div>
                        <div class="cell w-text">
                            <div class="t-mix"><a href="{{ URL::to('/services-detail') }}">Detailing</a></div>
                            <div class="bdy-mix">
                                <p>Id risus est pellentesque tristique cras malesuada ante varius. Tempor, mi elementum consectetur ut ante dignissim pulvinar sit. Amet nibh massa quam maecenas in sit. </p>
                            </div>
                        </div>
                        <div class="cell btn-mix">
                            <a href="{{ URL::to('/appointment') }}">
                                <button class="hvr-button" type="button">Book now</button>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="mix detailing">
                    <div class="tbl">
                        <div class="cell w-img">
                            <img src="{{ asset('images/mix2.jpg') }}" alt="" title=""/>
                        </div>
                        <div class="cell w-text">
                            <div class="t-mix"><a href="{{ URL::to('/services-detail') }}">Detailing</a></div>
                            <div class="bdy-mix">
                                <p>Id risus est pellentesque tristique cras malesuada ante varius. Tempor, mi elementum consectetur ut ante dignissim pulvinar sit. Amet nibh massa quam maecenas in sit. </p>
                            </div>
                        </div>
                        <div class="cell btn-mix">
                            <a href="{{ URL::to('/appointment') }}">
                                <button class="hvr-button" type="button">Book now</button>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="mix servicing">
                    <div class="tbl">
                        <div class="cell w-img">
                            <img src="{{ asset('images/mix1.jpg') }}" alt="" title=""/>
                        </div>
                        <div class="cell w-text">
                            <div class="t-mix"><a href="{{ URL::to('/services-detail') }}">Service & Maintenance</a></div>
                            <div class="bdy-mix">
                                <p>Id risus est pellentesque tristique cras malesuada ante varius. Tempor, mi elementum consectetur ut ante dignissim pulvinar sit. Amet nibh massa quam maecenas in sit. </p>
                            </div>
                        </div>
                        <div class="cell btn-mix">
                            <a href="{{ URL::to('/appointment') }}">
                                <button class="hvr-button" type="button">Book now</button>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="mix servicing">
                    <div class="tbl">
                        <div class="cell w-img">
                            <img src="{{ asset('images/mix1.jpg') }}" alt="" title=""/>
                        </div>
                        <div class="cell w-text">
                            <div class="t-mix"><a href="{{ URL::to('/services-detail') }}">Service & Maintenance</a></div>
                            <div class="bdy-mix">
                                <p>Id risus est pellentesque tristique cras malesuada ante varius. Tempor, mi elementum consectetur ut ante dignissim pulvinar sit. Amet nibh massa quam maecenas in sit. </p>
                            </div>
                        </div>
                        <div class="cell btn-mix">
                            <a href="{{ URL::to('/appointment') }}">
                                <button class="hvr-button" type="button">Book now</button>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="mix dealership">
                    <div class="tbl">
                        <div class="cell w-img">
                            <img src="{{ asset('images/mix3.jpg') }}" alt="" title=""/>
                        </div>
                        <div class="cell w-text">
                            <div class="t-mix"><a href="{{ URL::to('/services-detail') }}">Car Dealership</a></div>
                            <div class="bdy-mix">
                                <p>Id risus est pellentesque tristique cras malesuada ante varius. Tempor, mi elementum consectetur ut ante dignissim pulvinar sit. Amet nibh massa quam maecenas in sit. </p>
                            </div>
                        </div>
                        <div class="cell btn-mix">
                            <a href="{{ URL::to('/appointment') }}">
                                <button class="hvr-button" type="button">Book now</button>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="mix dealership">
                    <div class="tbl">
                        <div class="cell w-img">
                            <img src="{{ asset('images/mix3.jpg') }}" alt="" title=""/>
                        </div>
                        <div class="cell w-text">
                            <div class="t-mix"><a href="{{ URL::to('/services-detail') }}">Car Dealership</a></div>
                            <div class="bdy-mix">
                                <p>Id risus est pellentesque tristique cras malesuada ante varius. Tempor, mi elementum consectetur ut ante dignissim pulvinar sit. Amet nibh massa quam maecenas in sit. </p>
                            </div>
                        </div>
                        <div class="cell btn-mix">
                            <a href="{{ URL::to('/appointment') }}">
                                <button class="hvr-button" type="button">Book now</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <ul class="l-pagination">
                <li><a>Previous</a></li>
                <li class="mr5">/</li>
                <li><a class="active">1</a></li>
                <li><a>2</a></li>
                <li><a>3</a></li>
                <li><a>4</a></li>
                <li><a>...</a></li>
                <li class="mr5">/</li>
                <li><a>Next</a></li>
            </ul>
        </div>
    </div>

@endsection

@section('js')
<script type="text/javascript" src="{{ asset('js/mixitup.js') }}"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('.nav-services').addClass('active');

        var containerEl = document.querySelector('.box-services');

        var mixer = mixitup('.box-services');
	});
</script>
@endsection