<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Print Invoice</title>
    <link href="{{ asset('css/fonts.css') }}" rel="stylesheet"/>
    <style type="text/css" media="screen">
        body{ background: none; }
    </style>
</head>
<body style="color: #494949; font-family: 'Quicksand-Regular';" onload="window.print()">
    <div style="border-bottom: 2px solid #ACACAC; padding-bottom: 10px; margin-bottom: 30px;">
        <table style="border-collapse: collapse; width: 100%;">
            <tbody>
                <tr>
                    <td width="400" style="vertical-align:top;">
                        <a href="#" style="display: inline-block;">
                            <img src="{{ asset('images/logo-invoice.png') }}" style="width: 220px;" />
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="margin-bottom: 50px;">
        <table style="border-collapse: collapse; width: 100%;">
            <tbody>
                <tr>
                    <td style="width: 200px; line-height: 1;">
                        <div style="font-size: 11px; line-height: 14px; color: #777777; text-transform:uppercase; margin-bottom: 10px;">Bill to:</div>
                        <div style="font-size: 13px; line-height: 18px; color: black;">
                            <p style="margin: 0 0 5px; font-weight: bold;">Jonathan Tan</p>
                            <p style="margin: 0;">123 Jalan Dusuk</p>
                            <p style="margin: 0 0 5px;">Singapore 456009</p>
                            <p style="margin: 0;">jonathantan@gmail.com</p>
                        </div>
                    </td>
                    <td style="width: 200px; line-height: 1; text-align: right;">
                        <div style="font-size: 11px; line-height: 14px; color: #777777; text-transform:uppercase; margin-bottom: 10px;">invoice:</div>
                        <div style="font-size: 13px; line-height: 18px; color: black;">
                            <p style="margin: 0 0 5px; font-weight: bold;">Invoice Number: 0029389</p>
                            <p style="margin: 0 0 5px;">12 February 2022</p>
                            <p style="margin: 0;">Visit Time: 03:00PM</p>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="margin-bottom: 20px;">
        <table style="border-collapse: collapse; width: 100%;">
            <tbody>
                <tr>
                    <td>
                        <img src="{{ asset('images/bg-invoice.jpg') }}" style="width: 100%;" />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div>
        <table style="border-collapse: collapse; width: 100%; line-height: 1;">
            <thead>
                <tr>
                    <td style="font-size: 12px; padding: 0 10px 20px 0; color: #777777; width: 100px;">QUANTITY</td>
                    <td style="font-size: 12px; padding: 0 10px 20px; color: #777777;">DESCRIPTION</td>
                    <td style="font-size: 12px; padding: 0 10px 20px; color: #777777; width: 90px;">UNIT PRICE</td>
                    <td style="width: 90px; font-size: 12px; text-align: right; padding: 0 0 20px 10px;color: #777777;">PRICE</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="font-weight: bold; font-size: 12px; padding: 0 10px 10px 0; color: black; width: 100px;">1</td>
                    <td style="font-weight: bold; font-size: 12px; padding: 0 10px 10px; color: black;">Car wash & Vacuum</td>
                    <td style="font-weight: bold; font-size: 12px; padding: 0 10px 10px; color: black; width: 90px;">S$ 500.00</td>
                    <td style="font-weight: bold; width: 90px; font-size: 12px; text-align: right; padding: 0 0 20px 10px;color: black;">S$ 500.00</td>
                </tr>
                <tr>
                    <td style="font-weight: bold; font-size: 12px; padding: 0 10px 10px 0; color: black; width: 100px;">1</td>
                    <td style="font-weight: bold; font-size: 12px; padding: 0 10px 10px; color: black;">Car wash & Vacuum</td>
                    <td style="font-weight: bold; font-size: 12px; padding: 0 10px 10px; color: black; width: 90px;">S$ 500.00</td>
                    <td style="font-weight: bold; width: 90px; font-size: 12px; text-align: right; padding: 0 0 20px 10px;color: black;">S$ 500.00</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div>
        <table style="border-collapse: collapse; width: 100%; line-height: 1; border-bottom: 1px solid #ACACAC; border-top: 1px solid #ACACAC;">
            <tbody>
                <tr>
                    <td style="font-size: 12px; padding: 20px 10px 0 0; color: #777; width: 100px; text-transform: uppercase;">SUBTOTAL</td>
                    <td style="font-weight: bold; text-align: right; width: 50%; font-size: 12px; padding: 20px 0 0 10px; color: black;">S$ 5500.00</td>
                </tr>
                <tr>
                    <td style="font-size: 12px; padding: 20px 10px 20px 0; color: #777; width: 100px; text-transform: uppercase;">(+) TAX</td>
                    <td style="font-weight: bold; text-align: right; width: 50%; font-size: 12px; padding: 20px 0 20px 10px; color: black;">S$ 200.00</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div style="margin-bottom: 70px;">
        <table style="border-collapse: collapse; width: 100%; line-height: 1;">
            <tbody>
                <tr>
                    <td style="font-size: 12px; padding: 20px 10px 0 0; color: #777; width: 100px; text-transform: uppercase;">TOTAL</td>
                    <td style="font-weight: bold; text-align: right; width: 50%; font-size: 12px; padding: 20px 0 0 10px; color: black;">S$ 5700.00</td>
                </tr>
            </tbody>
        </table>
    </div>

    <table style="border-collapse: collapse; width: 100%;">
        <tbody>
            <tr>
                <div style="font-style: 14px; line-height: 20px;">
                    <p style="margin: 0 0 15px;">Note:</p>
                    <p style="margin: 0 0 10px;">Egestas ultrices id non senectus donec pellentesque convallis dignissim. Tincidunt accumsan nibh euismod libero dolor. Gravida mi gravida at cursus dui, arcu, sed ullamcorper mollis. Consectetur nisl massa amet augue praesent ut sagittis tellus tellus. Interdum arcu, convallis ultrices consectetur dignissim enim enim. Iaculis non massa placerat enim.</p>
                    <p style="margin: 0 0 10px;">Fringilla sit sapien placerat malesuada scelerisque dolor lectus augue non. Est gravida aliquet urna, varius convallis tincidunt. A faucibus et risus, purus mauris urna. Euismod pretium ultrices semper tellus neque, elementum proin sed. Lacus nisi commodo metus suscipit pellentesque sagittis quis dolor. Lobortis arcu, sed morbi magnis. Quis egestas proin viverra tincidunt sit vel massa.</p>
                    <p style="margin: 0;">Adipiscing morbi vel vitae malesuada pellentesque viverra. Elementum et vulputate diam, libero pellentesque egestas dui sem. Egestas massa viverra malesuada sagittis convallis eget.</p>
                </div>
            </tr>
        </tbody>
    </table>
</body>
</html>