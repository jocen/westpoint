@extends('layout')

@section('content')
    <div class="banner-global" style="background: url('images/banner-used-cars-detail.jpg') no-repeat center;">
        <div class="css-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="{{ URL::to('/') }}">Home</a></li>
                    <li>/</li>
                    <li><a href="{{ URL::to('/used-cars') }}">Buy & Sell Cars</a></li>
                    <li>/</li>
                    <li><a href="{{ URL::to('/used-cars') }}">Buy Used Cars</a></li>
                    <li>/</li>
                    <li><a href="{{ URL::to('/used-cars-detail') }}">Used Car Name</a></li>
                </ul>
            </div>
        </div>
        <div class="tbl">
            <div class="cell">
                <div class="container">
                    <div class="title">Very Long Product Name</div>
                    <div class="price">S$ 560 000</div>
                    <div class="bdy">
                        <p>Id risus est pellentesque tristique cras malesuada ante varius. Tempor, mi elementum consectetur ut ante dignissim pulvinar sit.</p>
                    </div>
                    <div class="link">
                        <a href="{{ URL::to('/appointment') }}"><button class="hvr-button" type="button">BOOK A TEST DRIVE</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="css-buy css-buy-detail">
        <div class="container">
            <div class="pad60">
                <div class="row">
                    <div class="col-md-5 col-lg-6 col-xl-7">
                        <div class="img"><img src="{{ asset('images/use-cars.jpg') }}" alt="" title=""/></div>
                    </div>
                    <div class="col-md-7 col-lg-6 col-xl-5">
                        <div class="price">S$ 560,000</div>
                        <div class="box-desc">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="nm">KIA Cerato 1.6A EX</div>
                                    <div class="txt"><img src="{{ asset('images/mileage.png') }}" alt="" title=""/> Mileage: 3,700 KM</div>
                                    <div class="txt"><img src="{{ asset('images/depreciation.png') }}" alt="" title=""/> Depreciation: S$ 9,000/year</div>
                                    <div class="txt"><img src="{{ asset('images/registered.png') }}" alt="" title=""/> Registered: 20 August 2021</div>
                                </div>
                                <div class="col-md-4">
                                    <div class="txt"><img src="{{ asset('images/owner.png') }}" alt="" title=""/> 1 Owner</div>
                                </div>
                            </div>
                        </div>
                        <div class="link">
                            <a href="{{ URL::to('/appointment') }}"><button class="hvr-button" type="button">BOOK A TEST DRIVE</button></a>
                        </div>
                        <div class="date">Listed 5 days ago</div>
                    </div>
                </div>
            </div>
            <ul class="l-buy">
                <li>
                    <a class="active" data-buy="details">Details</a>
                </li>
                <li class="nav-item">
                    <a data-buy="key-features">Key Features</a>
                </li>
                <li class="nav-item">
                    <a data-buy="accessories">Accessories</a>
                </li>
                <li class="nav-item">
                    <a data-buy="description">Description</a>
                </li>
            </ul>
            <section class="box-content-buy" id="details" data-anchor="details">
                <div class="t">Details</div>
                <div class="row">
                    <div class="col-6 col-md-4 col-lg-3">
                        <div class="item-details">
                            <div class="t-details">Transmission</div>
                            <div class="t2-details">Auto</div>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-3">
                        <div class="item-details">
                            <div class="t-details">COE</div>
                            <div class="t2-details">2500</div>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-xl-3">
                        <div class="item-details">
                            <div class="t-details">Dereg Value</div>
                            <div class="t2-details">2500</div>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-2 offset-xl-1">
                        <div class="item-details">
                            <div class="t-details">Fuel Type</div>
                            <div class="t2-details">Diesel</div>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-3">
                        <div class="item-details">
                            <div class="t-details">Open Market Value (OMV)</div>
                            <div class="t2-details">2500</div>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-3">
                        <div class="item-details">
                            <div class="t-details">Engine Cap</div>
                            <div class="t2-details">2500</div>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-xl-3">
                        <div class="item-details">
                            <div class="t-details">Additional Registration Fee (ARF)</div>
                            <div class="t2-details">2500</div>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-2 offset-xl-1">
                        <div class="item-details">
                            <div class="t-details">Curb Weight</div>
                            <div class="t2-details">2500</div>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-3">
                        <div class="item-details">
                            <div class="t-details">Power</div>
                            <div class="t2-details">2500</div>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-3">
                        <div class="item-details">
                            <div class="t-details">Type of Vehicle</div>
                            <div class="t2-details">Type placeholder</div>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-xl-3">
                        <div class="item-details">
                            <div class="t-details">Manufactured</div>
                            <div class="t2-details">25 December 2005</div>
                        </div>
                    </div>
                    <div class="col-6 col-md-4 col-lg-2 offset-xl-1">
                        <div class="item-details">
                            <div class="t-details">Road Tax</div>
                            <div class="t2-details">2500</div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="box-content-buy" id="key-features" data-anchor="key-features">
                <div class="t">Key Features</div>
                <div class="box-features">
                    <ul class="l-features">
                        <li>Powerful 2.0L In-Line 4 Cylinder TwinPower Turbo-Charged Engine</li>
                        <li>Responsive 8 Speed Auto Transmission With Paddle Shifter</li>
                        <li>ABS, ESP, SRS Airbags x 10.</li>
                    </ul>
                </div>
            </section>
            <section class="box-content-buy" id="accessories" data-anchor="accessories">
                <div class="t">Accessories</div>
                <div class="row row30">
                    <div class="col-md-4">
                        <div class="item-accessories">Sports Rims</div>
                    </div>
                    <div class="col-md-4">
                        <div class="item-accessories">Leather Seats</div>
                    </div>
                    <div class="col-md-4">
                        <div class="item-accessories">Cruise Control</div>
                    </div>
                    <div class="col-md-4">
                        <div class="item-accessories">Ambient Lightings</div>
                    </div>
                    <div class="col-md-4">
                        <div class="item-accessories">GPS</div>
                    </div>
                    <div class="col-md-4">
                        <div class="item-accessories">Auto Tail-Gate</div>
                    </div>
                    <div class="col-md-4">
                        <div class="item-accessories">Wireless Charging</div>
                    </div>
                    <div class="col-md-4">
                        <div class="item-accessories">Multimedia Interface</div>
                    </div>
                </div>
            </section>
            <section class="box-content-buy" id="description" data-anchor="description">
                <div class="t">Description</div>
                <div class="bdy-desc">
                    <p>Tellus sem cras aliquet porttitor non, neque dui, amet faucibus. Duis vel quam quis vestibulum. Integer proin quam parturient sed hendrerit vitae quam facilisis. Quam convallis feugiat cursus purus aliquam semper.</p>
                </div>
            </section>
            <div class="box-content-buy">
                <div class="t">Product Gallery</div>
                <div class="slider-gallery">
                    <div class="item-gallery-big">
                        <img src="{{ asset('images/buy-gallery1.jpg') }}" alt="" title=""/>
                    </div>
                    <div class="item-gallery-big">
                        <img src="{{ asset('images/buy-gallery2.jpg') }}" alt="" title=""/>
                    </div>
                    <div class="item-gallery-big">
                        <img src="{{ asset('images/buy-gallery3.jpg') }}" alt="" title=""/>
                    </div>
                    <div class="item-gallery-big">
                        <img src="{{ asset('images/buy-gallery4.jpg') }}" alt="" title=""/>
                    </div>
                    <div class="item-gallery-big">
                        <img src="{{ asset('images/buy-gallery5.jpg') }}" alt="" title=""/>
                    </div>
                    <div class="item-gallery-big">
                        <img src="{{ asset('images/buy-gallery1.jpg') }}" alt="" title=""/>
                    </div>
                    <div class="item-gallery-big">
                        <img src="{{ asset('images/buy-gallery2.jpg') }}" alt="" title=""/>
                    </div>
                    <div class="item-gallery-big">
                        <img src="{{ asset('images/buy-gallery3.jpg') }}" alt="" title=""/>
                    </div>
                </div>
                <div class="slider-thumb-gallery">
                    <div class="item-gallery">
                        <img src="{{ asset('images/buy-gallery1.jpg') }}" alt="" title=""/>
                    </div>
                    <div class="item-gallery">
                        <img src="{{ asset('images/buy-gallery2.jpg') }}" alt="" title=""/>
                    </div>
                    <div class="item-gallery">
                        <img src="{{ asset('images/buy-gallery3.jpg') }}" alt="" title=""/>
                    </div>
                    <div class="item-gallery">
                        <img src="{{ asset('images/buy-gallery4.jpg') }}" alt="" title=""/>
                    </div>
                    <div class="item-gallery">
                        <img src="{{ asset('images/buy-gallery5.jpg') }}" alt="" title=""/>
                    </div>
                    <div class="item-gallery">
                        <img src="{{ asset('images/buy-gallery1.jpg') }}" alt="" title=""/>
                    </div>
                    <div class="item-gallery">
                        <img src="{{ asset('images/buy-gallery2.jpg') }}" alt="" title=""/>
                    </div>
                    <div class="item-gallery">
                        <img src="{{ asset('images/buy-gallery3.jpg') }}" alt="" title=""/>
                    </div>
                </div>
            </div>
            <div class="box-content-buy">
                <div class="t">You may also like</div>
                <div class="slider-used-cars">
                    <div class="item-used">
                        <a href="{{ URL::to('/used-cars-detail') }}">
                            <div class="pad20">
                                <div class="model">Model</div>
                            </div>
                            <div class="img"><img src="{{ asset('images/used1.jpg') }}" alt="" title=""/></div>
                            <div class="pad20">
                                <div class="nm">BMW M6 2012</div>
                                <div class="price">SGD 56,000</div>
                            </div>
                        </a>
                    </div>
                    <div class="item-used">
                        <a href="{{ URL::to('/used-cars-detail') }}">
                            <div class="pad20">
                                <div class="model">Model</div>
                            </div>
                            <div class="img"><img src="{{ asset('images/used1.jpg') }}" alt="" title=""/></div>
                            <div class="pad20">
                                <div class="nm">BMW M6 2012</div>
                                <div class="price">SGD 56,000</div>
                            </div>
                        </a>
                    </div>
                    <div class="item-used">
                        <a href="{{ URL::to('/used-cars-detail') }}">
                            <div class="pad20">
                                <div class="model">Model</div>
                            </div>
                            <div class="img"><img src="{{ asset('images/used1.jpg') }}" alt="" title=""/></div>
                            <div class="pad20">
                                <div class="nm">BMW M6 2012</div>
                                <div class="price">SGD 56,000</div>
                            </div>
                        </a>
                    </div>
                    <div class="item-used">
                        <a href="{{ URL::to('/used-cars-detail') }}">
                            <div class="pad20">
                                <div class="model">Model</div>
                            </div>
                            <div class="img"><img src="{{ asset('images/used1.jpg') }}" alt="" title=""/></div>
                            <div class="pad20">
                                <div class="nm">BMW M6 2012</div>
                                <div class="price">SGD 56,000</div>
                            </div>
                        </a>
                    </div>
                    <div class="item-used">
                        <a href="{{ URL::to('/used-cars-detail') }}">
                            <div class="pad20">
                                <div class="model">Model</div>
                            </div>
                            <div class="img"><img src="{{ asset('images/used1.jpg') }}" alt="" title=""/></div>
                            <div class="pad20">
                                <div class="nm">BMW M6 2012</div>
                                <div class="price">SGD 56,000</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script type="text/javascript">
	$(document).ready(function() {
        $('.nav-buy').addClass('active');

        $('ul.l-buy li a').click(function(event) {
            $('ul.l-buy li a').removeClass('active');
            $(this).addClass('active');
            var scrollAnchor = $(this).attr('data-buy'),
            scrollPoint = $('section[data-anchor="' + scrollAnchor + '"]').offset().top;
            $('body,html').animate({
                scrollTop: scrollPoint
            }, 500);
            return false;
        });

        $('.slider-used-cars').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            centerMode: false,
            focusOnSelect: true,
            infinite: false,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                    }
                }
            ]
        });

        $('.slider-gallery').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            asNavFor: '.slider-thumb-gallery',
            dots: false,
            arrows: true,
            centerMode: true,
            focusOnSelect: true,
            infinite: true,
            centerPadding: '200px',
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        centerPadding: '50px',
                    }
                }
            ]
        });

        $('.slider-thumb-gallery').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            asNavFor: '.slider-gallery',
            dots: false,
            arrows: false,
            centerMode: false,
            focusOnSelect: true,
            infinite: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 4,
                        vertical: false,
                        arrows: true
                    }
                }
            ]
        });
	});
</script>
@endsection