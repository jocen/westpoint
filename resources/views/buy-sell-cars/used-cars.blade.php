@extends('layout')

@section('content')
    <div class="banner-global" style="background: url('images/banner-used-cars.jpg') no-repeat center;">
        <div class="css-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="{{ URL::to('/') }}">Home</a></li>
                    <li>/</li>
                    <li><a href="{{ URL::to('/used-cars') }}">Buy & Sell Cars</a></li>
                    <li>/</li>
                    <li><a href="{{ URL::to('/used-cars') }}">Buy Used Cars</a></li>
                </ul>
            </div>
        </div>
        <div class="tbl">
            <div class="cell">
                <div class="container">
                    <div class="title">Buy Used Cars</div>
                    <div class="bdy">
                        <p>Id risus est pellentesque tristique cras malesuada ante varius. Tempor, mi elementum consectetur ut ante dignissim pulvinar sit. Amet nibh massa quam maecenas in sit. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="css-buy">
        <div class="container">
            <div class="row row5">
                <div class="col-md-6 my-auto">
                    <div class="item active left">
                        <div class="img img1"><img src="{{ asset('images/used.png') }}" alt="" title=""/></div>
                        <div class="img img2"><img src="{{ asset('images/used2.png') }}" alt="" title=""/></div>
                        <div class="link">
                            <a href="{{ URL::to('/used-cars') }}"><button class="hvr-button" type="button">Browse Our Used Cars</button></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 my-auto">
                    <div class="item right">
                        <div class="img img1"><img src="{{ asset('images/sell.png') }}" alt="" title=""/></div>
                        <div class="img img2"><img src="{{ asset('images/sell2.png') }}" alt="" title=""/></div>
                        <div class="link">
                            <a href="{{ URL::to('/sell-car') }}"><button class="hvr-button" type="button">sell your car to us</button></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pad80">
                <div class="title-global">Browse Used Cars</div>
                <div class="result">Showing 129 results</div>
                <div class="row resp-row5" id="myScroll">
                    <div class="col-6 col-md-4 col-lg-3">
                        <div class="item-used">
                            <a href="{{ URL::to('/used-cars-detail') }}">
                                <div class="pad20">
                                    <div class="model">Model</div>
                                </div>
                                <div class="img"><img src="{{ asset('images/used1.jpg') }}" alt="" title=""/></div>
                                <div class="pad20">
                                    <div class="nm">BMW M6 2012</div>
                                    <div class="price">SGD 56,000</div>
                                </div>
                            </a>
                        </div> 
                    </div>
                    <div class="col-6 col-md-4 col-lg-3">
                        <div class="item-used">
                            <a href="{{ URL::to('/used-cars-detail') }}">
                                <div class="pad20">
                                    <div class="model">Model</div>
                                </div>
                                <div class="img"><img src="{{ asset('images/used2.jpg') }}" alt="" title=""/></div>
                                <div class="pad20">
                                    <div class="nm">BMW M6 2012</div>
                                    <div class="price">SGD 56,000</div>
                                </div>
                            </a>
                        </div> 
                    </div>
                    <div class="col-6 col-md-4 col-lg-3">
                        <div class="item-used">
                            <a href="{{ URL::to('/used-cars-detail') }}">
                                <div class="pad20">
                                    <div class="model">Model</div>
                                </div>
                                <div class="img"><img src="{{ asset('images/used3.jpg') }}" alt="" title=""/></div>
                                <div class="pad20">
                                    <div class="nm">BMW M6 2012</div>
                                    <div class="price">SGD 56,000</div>
                                </div>
                            </a>
                        </div> 
                    </div>
                    <div class="col-6 col-md-4 col-lg-3">
                        <div class="item-used">
                            <a href="{{ URL::to('/used-cars-detail') }}">
                                <div class="pad20">
                                    <div class="model">Model</div>
                                </div>
                                <div class="img"><img src="{{ asset('images/used4.jpg') }}" alt="" title=""/></div>
                                <div class="pad20">
                                    <div class="nm">BMW M6 2012</div>
                                    <div class="price">SGD 56,000</div>
                                </div>
                            </a>
                        </div> 
                    </div>
                    <div class="col-6 col-md-4 col-lg-3">
                        <div class="item-used">
                            <a href="{{ URL::to('/used-cars-detail') }}">
                                <div class="pad20">
                                    <div class="model">Model</div>
                                </div>
                                <div class="img"><img src="{{ asset('images/used1.jpg') }}" alt="" title=""/></div>
                                <div class="pad20">
                                    <div class="nm">BMW M6 2012</div>
                                    <div class="price">SGD 56,000</div>
                                </div>
                            </a>
                        </div> 
                    </div>
                    <div class="col-6 col-md-4 col-lg-3">
                        <div class="item-used">
                            <a href="{{ URL::to('/used-cars-detail') }}">
                                <div class="pad20">
                                    <div class="model">Model</div>
                                </div>
                                <div class="img"><img src="{{ asset('images/used2.jpg') }}" alt="" title=""/></div>
                                <div class="pad20">
                                    <div class="nm">BMW M6 2012</div>
                                    <div class="price">SGD 56,000</div>
                                </div>
                            </a>
                        </div> 
                    </div>
                    <div class="col-6 col-md-4 col-lg-3">
                        <div class="item-used">
                            <a href="{{ URL::to('/used-cars-detail') }}">
                                <div class="pad20">
                                    <div class="model">Model</div>
                                </div>
                                <div class="img"><img src="{{ asset('images/used3.jpg') }}" alt="" title=""/></div>
                                <div class="pad20">
                                    <div class="nm">BMW M6 2012</div>
                                    <div class="price">SGD 56,000</div>
                                </div>
                            </a>
                        </div> 
                    </div>
                    <div class="col-6 col-md-4 col-lg-3">
                        <div class="item-used">
                            <a href="{{ URL::to('/used-cars-detail') }}">
                                <div class="pad20">
                                    <div class="model">Model</div>
                                </div>
                                <div class="img"><img src="{{ asset('images/used4.jpg') }}" alt="" title=""/></div>
                                <div class="pad20">
                                    <div class="nm">BMW M6 2012</div>
                                    <div class="price">SGD 56,000</div>
                                </div>
                            </a>
                        </div> 
                    </div>
                    <div class="col-6 col-md-4 col-lg-3">
                        <div class="item-used">
                            <a href="{{ URL::to('/used-cars-detail') }}">
                                <div class="pad20">
                                    <div class="model">Model</div>
                                </div>
                                <div class="img"><img src="{{ asset('images/used1.jpg') }}" alt="" title=""/></div>
                                <div class="pad20">
                                    <div class="nm">BMW M6 2012</div>
                                    <div class="price">SGD 56,000</div>
                                </div>
                            </a>
                        </div> 
                    </div>
                    <div class="col-6 col-md-4 col-lg-3">
                        <div class="item-used">
                            <a href="{{ URL::to('/used-cars-detail') }}">
                                <div class="pad20">
                                    <div class="model">Model</div>
                                </div>
                                <div class="img"><img src="{{ asset('images/used2.jpg') }}" alt="" title=""/></div>
                                <div class="pad20">
                                    <div class="nm">BMW M6 2012</div>
                                    <div class="price">SGD 56,000</div>
                                </div>
                            </a>
                        </div> 
                    </div>
                    <div class="col-6 col-md-4 col-lg-3">
                        <div class="item-used">
                            <a href="{{ URL::to('/used-cars-detail') }}">
                                <div class="pad20">
                                    <div class="model">Model</div>
                                </div>
                                <div class="img"><img src="{{ asset('images/used3.jpg') }}" alt="" title=""/></div>
                                <div class="pad20">
                                    <div class="nm">BMW M6 2012</div>
                                    <div class="price">SGD 56,000</div>
                                </div>
                            </a>
                        </div> 
                    </div>
                    <div class="col-6 col-md-4 col-lg-3">
                        <div class="item-used">
                            <a href="{{ URL::to('/used-cars-detail') }}">
                                <div class="pad20">
                                    <div class="model">Model</div>
                                </div>
                                <div class="img"><img src="{{ asset('images/used4.jpg') }}" alt="" title=""/></div>
                                <div class="pad20">
                                    <div class="nm">BMW M6 2012</div>
                                    <div class="price">SGD 56,000</div>
                                </div>
                            </a>
                        </div> 
                    </div>
                </div>
                <div class="link">
                    <a><button type="button" class="hvr-button black"><i class="fas fa-redo" aria-hidden="true"></i> Load More</button></a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script type="text/javascript">
    var counter=0;
    $(window).scroll(function () {
        if ($(window).scrollTop() == $(document).height() - $(window).height() && counter < 2) {
            appendData();
        }
    });
    function appendData() {
        var html = '';
        for (i = 0; i < 10; i++) {
            html += '<div class="col-6 col-md-4 col-lg-3"><div class="item-used"><a href="{{ URL::to('/used-cars-detail') }}"><div class="pad20"><div class="model">Model</div></div><div class="img"><img src="{{ asset('images/used1.jpg') }}" alt="" title=""/></div><div class="pad20"><div class="nm">BMW M6 2012</div><div class="price">SGD 56,000</div></div></a></div></div>';
        }
        $('#myScroll').append(html);
        counter++;
        $('.link').hide();
        // if(counter==2)
        // $('#myScroll').append('<button id="uniqueButton" style="margin-left: 50%; background-color: powderblue;">Click</button><br /><br />');
    }
	$(document).ready(function() {
        $('.nav-buy').addClass('active');
	});
</script>
@endsection