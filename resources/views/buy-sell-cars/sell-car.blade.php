@extends('layout')

@section('content')
    <div class="banner-global" style="background: url('images/banner-used-cars.jpg') no-repeat center;">
        <div class="css-breadcrumb">
            <div class="container">
                <ul>
                    <li><a href="{{ URL::to('/') }}">Home</a></li>
                    <li>/</li>
                    <li><a href="{{ URL::to('/used-cars') }}">Buy & Sell Cars</a></li>
                    <li>/</li>
                    <li><a href="{{ URL::to('/sell-car') }}">Sell Your Car</a></li>
                </ul>
            </div>
        </div>
        <div class="tbl">
            <div class="cell">
                <div class="container">
                    <div class="title">Sell Your Car</div>
                </div>
            </div>
        </div>
    </div>
    <div class="css-buy">
        <div class="container">
            <div class="row row5">
                <div class="col-md-6 my-auto">
                    <div class="item left">
                        <div class="img img1"><img src="{{ asset('images/used.png') }}" alt="" title=""/></div>
                        <div class="img img2"><img src="{{ asset('images/used2.png') }}" alt="" title=""/></div>
                        <div class="link">
                            <a href="{{ URL::to('/used-cars') }}"><button class="hvr-button" type="button">Browse Our Used Cars</button></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 my-auto">
                    <div class="item active right">
                        <div class="img img1"><img src="{{ asset('images/sell.png') }}" alt="" title=""/></div>
                        <div class="img img2"><img src="{{ asset('images/sell2.png') }}" alt="" title=""/></div>
                        <div class="link">
                            <a href="{{ URL::to('/sell-car') }}"><button class="hvr-button" type="button">sell your car to us</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="banner-sell" style="background: url('images/banner-sell-car.jpg') no-repeat center;">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 my-auto">
                        <div class="t-sell">Sell your car to us</div>
                        <ul class="l-sell">
                            <li>Montes, aenean ut magna tempus amet, non eu tincidunt morbi.</li>
                            <li>Lacus, rhoncus, feugiat quisque posuere velit quis.</li>
                            <li>Sed volutpat et leo feugiat aenean eleifend praesent.</li>
                        </ul>
                    </div>
                    <div class="col-md-7 my-auto">
                        <div class="box-sell">
                            <div class="t">Get the best price for your car today!</div>
                            <form action="{{ URL::to('/') }}">
                                <div class="form-group">
                                    <label id="name">*Name</label>
                                    <input class="form-control" id="name" name="name" type="text" required="" />
                                </div>
                                <div class="form-group">
                                    <label id="number">*Contact Number</label>
                                    <input class="form-control" id="number" name="number" type="text" required="" />
                                </div>
                                <div class="form-group">
                                    <label id="email">*Email</label>
                                    <input class="form-control" id="email" name="email" type="email" required="" />
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label id="car-make">*Car Make</label>
                                            <input class="form-control" id="car-make" name="car-make" type="text" required="" />
                                        </div>     
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label id="car-model">*Car Model</label>
                                            <input class="form-control" id="car-model" name="car-model" type="text" required="" />
                                        </div>     
                                    </div>
                                </div>
                                <button class="hvr-button full100" type="submit">Get Quote</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function() {
        $('.nav-buy').addClass('active');
    });
</script>
@endsection