@extends('layout')

@section('content')
    
    <div class="css-auth">
        <div class="banner" style="background: url('images/banner-login.jpg') no-repeat center;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 offset-lg-1">
                        <div class="title">Recover Password</div>
                        <div class="text-auth">Please enter your new password below.</div>
                        <form>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="new-pass">New Password:</label>
                                        <input class="form-control" id="new-pass" name="new-pass" type="password" required="" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="confirm-pass">Confirm Password:</label>
                                        <input class="form-control" id="confirm-pass" name="confirm-pass" type="password" required="" />
                                    </div>
                                </div>
                            </div>
                            <button class="hvr-button" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script type="text/javascript">
	$(document).ready(function() {

	});
</script>
@endsection