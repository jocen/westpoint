@extends('layout')

@section('content')
    
    <div class="css-auth">
        <div class="banner" style="background: url('images/banner-login.jpg') no-repeat center;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 offset-lg-1">
                        <div class="title mb20">Forgot Password</div>
                        <div class="text-auth">Don't worry, you can get your new password by filling your existing email address. We will send you new one.</div>
                        <form>
                            <div class="row">
                                <div class="col-md-8 col-lg-7">
                                    <div class="form-group">
                                        <label id="email">Email:</label>
                                        <input class="form-control" id="email" name="email" type="text" required="" />
                                    </div>
                                    <button class="hvr-button" type="button" data-bs-toggle="modal" data-bs-target="#modal-forgot">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-forgot" class="modal fade modal-global" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="pad-pop">
                    <div class="pad-bdy">
                        <div class="img-success">
                            <img src="{{ asset('images/forgot.png') }}" alt="" title=""/>
                        </div>
                        <div class="t-pop">Password Reset</div>
                        <div class="t-pop2">
                            <p>We have sent an email to your registered email account. Do follow the instructions to reset your password.</p>
                        </div>
                        <div class="btn-pop">
                            <a href="{{ URL::to('/login') }}">
                                <button class="hvr-button" type="button">Ok, Got it</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
<script type="text/javascript">
	$(document).ready(function() {

	});
</script>
@endsection