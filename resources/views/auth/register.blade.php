@extends('layout')

@section('content')
    
    <div class="css-auth">
        <div class="banner" style="background: url('images/banner-login.jpg') no-repeat center;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 offset-lg-1">
                        <div class="t1">JOIN US</div>
                        <div class="title">Create new account</div>
                        <div class="text-auth">Already a member? <a href="{{ URL::to('/login') }}">Log In</a></div>
                        <ul class="l-auth">
                            <li><a href="#"><img src="{{ asset('images/google.png') }}" alt="" title=""/> Sign Up with Google</a></li>
                            <li><a href="#"><img src="{{ asset('images/fb.png') }}" alt="" title=""/> Sign Up with Facebook</a></li>
                        </ul>
                        <form>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="name">Name:</label>
                                        <input class="form-control" id="name" name="name" type="text" required=""/>
                                    </div>     
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="email">Email:</label>
                                        <input class="form-control" id="email" name="email" type="text" required=""/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="phone">Contact Number:</label>
                                        <input class="form-control only-number" id="phone" name="phone" type="text" required="" value=""/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="pass1">Password:</label>
                                        <input class="form-control" id="pass1" name="pass1" type="password" required="" value=""/>
                                    </div>    
                                </div>
                            </div><br/>
                            <button class="hvr-button" type="button" data-bs-toggle="modal" data-bs-target="#modal-signup">Sign Up</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-signup" class="modal fade modal-global" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="pad-pop">
                    <div class="pad-bdy">
                        <div class="img-success">
                            <img src="{{ asset('images/signup.png') }}" alt="" title=""/>
                        </div>
                        <div class="t-pop">Account Created!</div>
                        <div class="t-pop2">
                            <p>Montes, aenean ut magna tempus amet, non eu tincidunt morbi. Lacus, rhoncus, feugiat quisque posuere velit quis.</p><br/>
                            <p>Sed volutpat et leo feugiat aenean eleifend praesent.</p>
                        </div>
                        <div class="btn-pop">
                            <a href="{{ URL::to('/login') }}">
                                <button class="hvr-button" type="button">Ok, Got it</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
<script type="text/javascript">
	$(document).ready(function() {

	});
</script>
@endsection