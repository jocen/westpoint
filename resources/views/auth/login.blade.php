@extends('layout')

@section('content')
    
    <div class="css-auth">
        <div class="banner" style="background: url('images/banner-login.jpg') no-repeat center;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 offset-lg-1">
                        <div class="title">Login</div>
                        <div class="text-auth">Not a member? <a href="{{ URL::to('/register') }}">Create an account with us</a></div>
                        <ul class="l-auth">
                            <li><a href="#"><img src="{{ asset('images/google.png') }}" alt="" title=""/> Login with Google</a></li>
                            <li><a href="#"><img src="{{ asset('images/fb.png') }}" alt="" title=""/> Login with Facebook</a></li>
                        </ul>
                        <form action="{{ URL::to('/regular/dashboard') }}">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="email">Email:</label>
                                        <input class="form-control" id="email" name="email" type="text" required="" />
                                    </div>     
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="password">Password:</label>
                                        <input class="form-control" id="password" name="password" type="password" required="" />
                                    </div>     
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 offset-md-6">
                                    <div class="text-forgot"><a href="{{ URL::to('/forgot-password') }}">Forgot Password</a>?</div>
                                </div>
                            </div>
                            <button class="hvr-button" type="submit">Login</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
<script type="text/javascript">
	$(document).ready(function() {

	});
</script>
@endsection