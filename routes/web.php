<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

/* INFORMATION */
Route::get('/about-us', function () {
    return view('information/about-us');
});
Route::get('/gallery-reviews', function () {
    return view('information/gallery-reviews');
});
Route::get('/terms', function () {
    return view('information/terms');
});
Route::get('/privacy-policy', function () {
    return view('information/privacy-policy');
});

/* BUY & SELL CARS */
Route::get('/used-cars', function () {
    return view('buy-sell-cars/used-cars');
});
Route::get('/used-cars-detail', function () {
    return view('buy-sell-cars/used-cars-detail');
});
Route::get('/sell-car', function () {
    return view('buy-sell-cars/sell-car');
});

/* APPOINTMENT */
Route::get('/appointment', function () {
    return view('appointment/index');
});
Route::get('/appointment-confirm', function () {
    return view('appointment/confirm');
});

/* SERVICES */
Route::get('/services', function () {
    return view('services/index');
});
Route::get('/services-detail', function () {
    return view('services/detail');
});


/* PRODUCT */
Route::get('/product', function () {
    return view('product/index');
});
Route::get('/rims-detail', function () {
    return view('product/rims-detail');
});
Route::get('/accessories-detail', function () {
    return view('product/accessories-detail');
});
Route::get('/engine-detail', function () {
    return view('product/engine-detail');
});



/* AUTH */
Route::get('/login', function () {
    return view('auth/login');
});
Route::get('/register', function () {
    return view('auth/register');
});
Route::get('/forgot-password', function () {
    return view('auth/forgot-password');
});
Route::get('/recovery', function () {
    return view('auth/recovery');
});


/* MEMBER AREA */

/* REGULAR */
Route::get('/regular/dashboard', function () {
    return view('member/regular/dashboard');
});
Route::get('/regular/account-information', function () {
    return view('member/regular/account-information');
});
Route::get('/regular/my-bookings', function () {
    return view('member/regular/my-bookings');
});
Route::get('/regular/my-rewards', function () {
    return view('member/regular/my-rewards');
});
Route::get('/regular/rewards-catalogue', function () {
    return view('member/regular/rewards-catalogue');
});

/* BUSINESS */
Route::get('/business/dashboard', function () {
    return view('member/business/dashboard');
});
Route::get('/business/account-information', function () {
    return view('member/business/account-information');
});
Route::get('/business/my-bookings', function () {
    return view('member/business/my-bookings');
});


/* INVOICE */
Route::get('/invoice', function () {
    return view('invoice');
});